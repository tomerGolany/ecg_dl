import tensorflow as tf
from ecg_dl import read_data
import os
import numpy as np
import argparse
import pickle
from my_dl_lib import generic_lstm
from my_dl_lib.generic_gan import generator
from my_dl_lib import generic_dataset


def load_dataset_from_pickle(pickle_file_path):
    """

    :param pickle_file_path:
    :return:
    """
    pickle_in = open(pickle_file_path, "rb")
    print("Loading dataset from pickle file...")
    loaded_dataset = pickle.load(pickle_in)
    print("**Loaded done**")
    lstm_beats_train = loaded_dataset['Train_beats']
    train_tags = loaded_dataset['Train_tags']
    lstm_beats_val = loaded_dataset['Val_beats']
    validation_tags = loaded_dataset['Val_tags']
    lstm_beats_test = loaded_dataset['Test_beats']
    test_tags = loaded_dataset['Test_tags']
    return lstm_beats_train, train_tags, lstm_beats_val, validation_tags, lstm_beats_test, test_tags


def create_dataset_according_to_aami(positive_class,
                                     length_of_each_sample=5, save_to_pickle=False, pickle_path=None,
                                     print_statistics=False, drop_other_beats=False):
    """

    :param original_data_dir:
    :param positive_class:
    :param length_of_each_sample:
    :param save_to_pickle:
    :param pickle_path:
    :return:
    """
    train_set, validation_set, test_set = read_data.divide_data_to_train_test_according_to_aami()

    validation_beats, validation_tags = read_data.merge_data_of_patients(validation_set, binary_pred=True,
                                                                         postivie_class=positive_class, aami_standarts=True)

    train_beats, train_tags = read_data.merge_data_of_patients(train_set, binary_pred=True,
                                                               postivie_class=positive_class, aami_standarts=True)

    test_beats, test_tags = read_data.merge_data_of_patients(test_set, binary_pred=True,
                                                             postivie_class=positive_class, aami_standarts=True)

    if drop_other_beats:
        i = 0
        while i < len(train_beats):
            x = train_tags[i][1]
            if x != 'Q' and x != 'f' and x != '/' and x != 'F' and x != 'E' and x != 'V' and x != 'S' and x != 'J' \
                    and x != 'a' and x != 'A' and x != 'j' and x != 'e' and x != 'R' and x != 'L' and x != 'N':
                # del train_beats[i]
                # del train_tags[i]
                train_beats = np.delete(train_beats, [i], axis=0)
                train_tags = np.delete(train_tags, [i], axis=0)
                continue
            i += 1

        i = 0
        while i < len(validation_beats):
            x = validation_tags[i][1]
            if x != 'Q' and x != 'f' and x != '/' and x != 'F' and x != 'E' and x != 'V' and x != 'S' and x != 'J' \
                    and x != 'a' and x != 'A' and x != 'j' and x != 'e' and x != 'R' and x != 'L' and x != 'N':
                # del validation_beats[i]
                # del validation_tags[i]
                validation_beats = np.delete(validation_beats, [i], axis=0)
                validation_tags = np.delete(validation_tags, [i], axis=0)
                continue
            i += 1

        i = 0
        while i < len(test_beats):
            x = test_tags[i][1]
            if x != 'Q' and x != 'f' and x != '/' and x != 'F' and x != 'E' and x != 'V' and x != 'S' and x != 'J' \
                    and x != 'a' and x != 'A' and x != 'j' and x != 'e' and x != 'R' and x != 'L' and x != 'N':
                # del test_beats[i]
                # del test_tags[i]
                test_beats = np.delete(test_beats, [i], axis=0)
                test_tags = np.delete(test_tags, [i], axis=0)
                continue
            i += 1

    if print_statistics:
        print("Train set statistics:")
        specific_train_tags = [x[1] for x in train_tags]
        specific_validation_tags = [x[1] for x in validation_tags]

        # 1. NOR - N:
        num_NOR = len([1 for x in specific_train_tags if x == 'N']) + len(
            [1 for x in specific_validation_tags if x == 'N'])

        # 2.LBBB - L:
        num_LBBB = len([1 for x in specific_train_tags if x == 'L']) + len(
            [1 for x in specific_validation_tags if x == 'L'])

        # 3. RBBB - R:
        num_RBBB = len([1 for x in specific_train_tags if x == 'R']) + len(
            [1 for x in specific_validation_tags if x == 'R'])

        # 4. AE - e
        num_AE = len([1 for x in specific_train_tags if x == 'e']) + len(
            [1 for x in specific_validation_tags if x == 'e'])

        # 5. NE - j :
        num_NE = len([1 for x in specific_train_tags if x == 'j']) + len(
            [1 for x in specific_validation_tags if x == 'j'])

        print(" NOR: %d || LBBB: %d || RBBB: %d || AE: %d || NE: %d " % (num_NOR, num_LBBB, num_RBBB, num_AE, num_NE))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 6. AP - A:
        num_AP = len([1 for x in specific_train_tags if x == 'A']) + len(
            [1 for x in specific_validation_tags if x == 'A'])

        # 7. aAP - a:
        num_aAP = len([1 for x in specific_train_tags if x == 'a']) + len(
            [1 for x in specific_validation_tags if x == 'a'])

        # 8. NP - J:
        num_NP = len([1 for x in specific_train_tags if x == 'J']) + len(
            [1 for x in specific_validation_tags if x == 'J'])

        # 9. SP - S:
        num_SP = len([1 for x in specific_train_tags if x == 'S']) + len(
            [1 for x in specific_validation_tags if x == 'S'])

        print(" AP: %d || aAP: %d || NP: %d || SP: %d " % (num_AP, num_aAP, num_NP, num_SP))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 10. PVC - V:
        num_PVC = len([1 for x in specific_train_tags if x == 'V']) + len(
            [1 for x in specific_validation_tags if x == 'V'])

        # 11. VE - E:
        num_VE = len([1 for x in specific_train_tags if x == 'E']) + len(
            [1 for x in specific_validation_tags if x == 'E'])

        print(" PVC: %d || VE: %d " % (num_PVC, num_VE))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 12. fVN - F:
        num_fVN = len([1 for x in specific_train_tags if x == 'F']) + len(
            [1 for x in specific_validation_tags if x == 'F'])

        print(" fVN: %d " % (num_fVN))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 13. P - /:
        num_P = len([1 for x in specific_train_tags if x == '/']) + len(
            [1 for x in specific_validation_tags if x == '/'])

        # 14. fPN - f:
        num_fPN = len([1 for x in specific_train_tags if x == 'f']) + len(
            [1 for x in specific_validation_tags if x == 'f'])

        # 15. U - Q:
        num_U = len([1 for x in specific_train_tags if x == 'Q']) + len(
            [1 for x in specific_validation_tags if x == 'Q'])

        print(" P: %d || fPN: %d || U: %d " % (num_P, num_fPN, num_U))
        print("_____________________________________________________")
        print("_____________________________________________________")

        num_others = len([1 for x in specific_train_tags if x != 'Q' and x != 'f' and x != '/' and x != 'F'
                          and x != 'E' and x != 'V' and x != 'S' and x != 'J' and x != 'a' and x != 'A' and x != 'j'
                          and x != 'e' and x != 'R' and x != 'L' and x != 'N']) + len([1 for x in
                                                                                       specific_validation_tags if x != 'Q' and x != 'f' and x != '/' and x != 'F'
                          and x != 'E' and x != 'V' and x != 'S' and x != 'J' and x != 'a' and x != 'A' and x != 'j'
                          and x != 'e' and x != 'R' and x != 'L' and x != 'N'])

        print("Number of others: ", num_others)
        print("Total number of beats in train:", len(specific_train_tags) + len(specific_validation_tags))
        print("Number agg: ", num_fPN + num_fVN + num_SP + num_NP + num_aAP + num_AP + num_NE + num_AE + num_RBBB + num_LBBB + num_NOR + num_P + num_PVC + num_U + num_VE)
        print("*****************************************************")

        print("Test set statistics:")
        specific_test_tags = [x[1] for x in test_tags]

        # 1. NOR - N:
        num_NOR = len([1 for x in specific_test_tags if x == 'N'])

        # 2.LBBB - L:
        num_LBBB = len([1 for x in specific_test_tags if x == 'L'])

        # 3. RBBB - R:
        num_RBBB = len([1 for x in specific_test_tags if x == 'R'])

        # 4. AE - e
        num_AE = len([1 for x in specific_test_tags if x == 'e'])

        # 5. NE - j :
        num_NE = len([1 for x in specific_test_tags if x == 'j'])

        print(" NOR: %d || LBBB: %d || RBBB: %d || AE: %d || NE: %d " % (num_NOR, num_LBBB, num_RBBB, num_AE, num_NE))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 6. AP - A:
        num_AP = len([1 for x in specific_test_tags if x == 'A'])

        # 7. aAP - a:
        num_aAP = len([1 for x in specific_test_tags if x == 'a'])

        # 8. NP - J:
        num_NP = len([1 for x in specific_test_tags if x == 'J'])

        # 9. SP - S:
        num_SP = len([1 for x in specific_test_tags if x == 'S'])

        print(" AP: %d || aAP: %d || NP: %d || SP: %d " % (num_AP, num_aAP, num_NP, num_SP))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 10. PVC - V:
        num_PVC = len([1 for x in specific_test_tags if x == 'V'])

        # 11. VE - E:
        num_VE = len([1 for x in specific_test_tags if x == 'E'])

        print(" PVC: %d || VE: %d " % (num_PVC, num_VE))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 12. fVN - F:
        num_fVN = len([1 for x in specific_test_tags if x == 'F'])

        print(" fVN: %d " % (num_fVN))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 13. P - /:
        num_P = len([1 for x in specific_test_tags if x == '/'])

        # 14. fPN - f:
        num_fPN = len([1 for x in specific_test_tags if x == 'f'])

        # 15. U - Q:
        num_U = len([1 for x in specific_test_tags if x == 'Q'])

        print(" P: %d || fPN: %d || U: %d " % (num_P, num_fPN, num_U))
        print("_____________________________________________________")
        print("_____________________________________________________")

        num_others = len([1 for x in specific_test_tags if x != 'Q' and x != 'f' and x != '/' and x != 'F'
                          and x != 'E' and x != 'V' and x != 'S' and x != 'J' and x != 'a' and x != 'A' and x != 'j'
                          and x != 'e' and x != 'R' and x != 'L' and x != 'N'])

        print("Number of others: ", num_others)
        print("Total number of beats in test:", len(specific_test_tags))
        print("Number agg: ",
              num_fPN + num_fVN + num_SP + num_NP + num_aAP + num_AP + num_NE + num_AE + num_RBBB + num_LBBB + num_NOR
              + num_P + num_PVC + num_U + num_VE)

    validation_tags = np.array([int(x[0]) for x in validation_tags])
    train_tags = np.array([int(x[0]) for x in train_tags])
    test_tags = np.array([int(x[0]) for x in test_tags])

    train_beats = np.concatenate((train_beats, validation_beats), axis=0)
    print("shape train: ", train_beats.shape)
    train_tags = np.concatenate((train_tags, validation_tags), axis=0)
    print("shape tags", train_tags.shape)

    lstm_beats_train = [[beat[i:i + length_of_each_sample] for i in range(0, len(beat), length_of_each_sample)] for beat
                        in train_beats]
    lstm_beats_val = [[beat[i:i + length_of_each_sample] for i in range(0, len(beat), length_of_each_sample)] for beat
                      in validation_beats]
    lstm_beats_test = [[beat[i:i + length_of_each_sample] for i in range(0, len(beat), length_of_each_sample)] for beat
                       in test_beats]

    # Convert tags to one-hot vectors:
    val_dataset = generic_dataset.GenericDataSetIterator(validation_beats, validation_tags)
    validation_tags = val_dataset.convert_labels_to_one_hot_vector(2)
    train_dataset = generic_dataset.GenericDataSetIterator(train_beats, train_tags)
    train_tags = train_dataset.convert_labels_to_one_hot_vector(2)

    test_dataset = generic_dataset.GenericDataSetIterator(test_beats, test_tags)
    test_tags = test_dataset.convert_labels_to_one_hot_vector(2)

    if save_to_pickle:
        pickle_dict = {'Train_beats': np.array(lstm_beats_train), 'Train_tags': np.array(train_tags),
                       'Val_beats': np.array(lstm_beats_val),
                       'Val_tags': np.array(validation_tags), 'Test_beats': np.array(lstm_beats_test),
                       'Test_tags': np.array(test_tags)}
        assert pickle_path is not None
        pickle_out = open(pickle_path, "wb")
        pickle.dump(pickle_dict, pickle_out)
        pickle_out.close()
        print("Saved data into pickle file %s", pickle_path)

    return np.array(lstm_beats_train), np.array(train_tags), np.array(lstm_beats_val), np.array(validation_tags), \
           np.array(lstm_beats_test), np.array(test_tags)


def create_dataset(original_data_dir, number_of_classes, length_of_each_sample, save_to_pickle=False, pickle_path=None,
                   positive_class=None):
    """

    :param pickle_path:
    :param save_to_pickle:
    :param original_data_dir:
    :param number_of_classes:
    :param length_of_each_sample:
    :return:
    """
    # 1. Retrieve only the number of each patient and devide the patients to the 3 sets. Each patient has the same
    # number of beats.
    patient_numbers = read_data.get_list_of_all_patient_numbers(original_data_dir)
    train_set, validation_set, test_set = read_data.divide_data_to_train_val_test(patient_numbers, shuffle=False)
    if positive_class is None:
        validation_beats, validation_tags = read_data.merge_data_of_patients(validation_set, binary_pred=False)

        train_beats, train_tags = read_data.merge_data_of_patients(train_set, binary_pred=False)

        test_beats, test_tags = read_data.merge_data_of_patients(test_set, binary_pred=False)
    else:
        validation_beats, validation_tags = read_data.merge_data_of_patients(validation_set, binary_pred=True,
                                                                             postivie_class=positive_class)

        train_beats, train_tags = read_data.merge_data_of_patients(train_set, binary_pred=True,
                                                                   postivie_class=positive_class)

        test_beats, test_tags = read_data.merge_data_of_patients(test_set, binary_pred=True,
                                                                 postivie_class=positive_class)
        print("Size of test set: ", len(test_tags))
        print("In test beats - %d are positive", list(test_tags).count(1))
        print("In train beats - %d are positive", list(train_tags).count(1))

    # 2. Set each beat to fit as an entrance to the LSTM:
    lstm_beats_train = [[beat[i:i+length_of_each_sample] for i in range(0, len(beat), length_of_each_sample)] for beat
                        in train_beats]
    lstm_beats_val = [[beat[i:i + length_of_each_sample] for i in range(0, len(beat), length_of_each_sample)] for beat
                      in validation_beats]
    lstm_beats_test = [[beat[i:i + length_of_each_sample] for i in range(0, len(beat), length_of_each_sample)] for beat
                       in test_beats]

    # Convert tags to one-hot vectors:
    val_dataset = generic_dataset.GenericDataSetIterator(validation_beats, validation_tags)
    validation_tags = val_dataset.convert_labels_to_one_hot_vector(number_of_classes)
    train_dataset = generic_dataset.GenericDataSetIterator(train_beats, train_tags)
    train_tags = train_dataset.convert_labels_to_one_hot_vector(number_of_classes)

    test_dataset = generic_dataset.GenericDataSetIterator(test_beats, test_tags)
    test_tags = test_dataset.convert_labels_to_one_hot_vector(number_of_classes)

    if save_to_pickle:
        pickle_dict = {'Train_beats': np.array(lstm_beats_train), 'Train_tags': np.array(train_tags),
                       'Val_beats': np.array(lstm_beats_val),
                       'Val_tags': np.array(validation_tags), 'Test_beats': np.array(lstm_beats_test),
                       'Test_tags': np.array(test_tags)}
        assert pickle_path is not None
        pickle_out = open(pickle_path, "wb")
        pickle.dump(pickle_dict, pickle_out)
        pickle_out.close()
        print("Saved data into pickle file %s", pickle_path)

    return np.array(lstm_beats_train), np.array(train_tags), np.array(lstm_beats_val), np.array(validation_tags), \
           np.array(lstm_beats_test), np.array(test_tags)


def create_lstm_classifier(number_of_classes, sequence_length, length_of_each_sample, tensor_board_dir, save_model_dir,
                           dropout_prob, mode='TRAIN'):
    """

    :param number_of_classes:
    :param sequence_length:
    :param length_of_each_sample:
    :param tensor_board_dir:
    :param save_model_dir:
    :param dropout_prob:
    :param mode:
    :return:
    """
    ecg_lstm = generic_lstm.GenericLSTM(number_of_hidden_neurons=512, size_of_sequence=sequence_length,
                                        size_of_each_sample=length_of_each_sample,
                                        output_length=number_of_classes, number_of_lstm_layers=2,
                                        tensor_board_dir=tensor_board_dir, saved_models_dir=save_model_dir, mode=mode)
    # TODO: add dropout
    return ecg_lstm


def train(lstm_classifier, train_beats, train_tags, validation_beats, validation_tags, number_of_iterations,
          batch_size):
    print("Begin Training: ")
    print("Number of iterations: %d, batch size: %d " % (number_of_iterations, batch_size))
    print("Size of train set: %d , size of validation set %d" % (len(train_beats), len(validation_beats)))

    lstm_classifier.train(x_data=train_beats, y_labels=train_tags, x_val=validation_beats, y_val=validation_tags,
                          num_of_iterations=number_of_iterations, batch_size=batch_size,loss_function='cross_entropy',
                          optimizer='rms_prop',
                          word_to_index=None, index_to_word=None)
    print("Training Done")


def evaluate_classifier(ecg_classifier, test_beats, test_tags, positive_class='TRAIN'):
    """
    Run the classifier on the test set, and print results and graphs
    :param ecg_classifier:
    :param test_beats:
    :param test_tags:
    :param positive_class
    :return:
    """
    assert ecg_classifier.session is not None
    print("***********************************************************************************************************")
    print("Evaluating results on test set:")
    accuracy, cost, binary_predictions_of_test_set = ecg_classifier.eval_accuracy(test_beats, test_tags)

    print("***********************************************************************************")
    print("binary preds first 10:", binary_predictions_of_test_set[:10])
    print("***********************************************************************************")
    if positive_class == 'TRAIN':
        print("**************************Results on Normal Beats:******************************************************"
              "***")
        normal_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[1] == 1]
        number_of_correct_preds_among_normal_beats = 0
        for j in normal_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 1:
                number_of_correct_preds_among_normal_beats += 1
        print("Among %d normal beats, classifier predicted correctly %d of them" %
              (len(normal_beats_ids), number_of_correct_preds_among_normal_beats))
        print("Accuracy on normal beats:", number_of_correct_preds_among_normal_beats / len(normal_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on A Beats:*********************************************************")
        A_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[2] == 1]
        number_of_correct_preds_among_A_beats = 0
        for j in A_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 2:
                number_of_correct_preds_among_A_beats += 1
        print("Among %d A beats, classifier predicted correctly %d of them" %
              (len(A_beats_ids), number_of_correct_preds_among_A_beats))
        print("Accuracy on A beats:", number_of_correct_preds_among_A_beats / len(A_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on V Beats:*********************************************************")
        V_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[3] == 1]
        number_of_correct_preds_among_V_beats = 0
        for j in V_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 3:
                number_of_correct_preds_among_V_beats += 1
        print("Among %d V beats, classifier predicted correctly %d of them" %
              (len(V_beats_ids), number_of_correct_preds_among_V_beats))
        print("Accuracy on V beats:", number_of_correct_preds_among_V_beats / len(V_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on R Beats:*********************************************************")
        R_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[4] == 1]
        number_of_correct_preds_among_R_beats = 0
        for j in R_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 4:
                number_of_correct_preds_among_R_beats += 1
        print("Among %d R beats, classifier predicted correctly %d of them" %
              (len(R_beats_ids), number_of_correct_preds_among_R_beats))
        print("Accuracy on R beats:", number_of_correct_preds_among_R_beats / len(R_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on others Beats:******************************************************"
              "***")
        others_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[0] == 1]
        number_of_correct_preds_among_others_beats = 0
        for j in others_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 0:
                number_of_correct_preds_among_others_beats += 1
        print("Among %d others beats, classifier predicted correctly %d of them" %
              (len(others_beats_ids), number_of_correct_preds_among_others_beats))
        print("Accuracy on others beats:", number_of_correct_preds_among_others_beats / len(others_beats_ids))
        print("********************************************************************************************************"
              "***")

    else:
        positive_beat = positive_class.split('_')[1]
        print("**************************Results on " + positive_beat + " Beats:***************************************"
                                                                        "******************")
        positive_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[1] == 1]
        number_of_correct_preds_among_postive_beats = 0
        for j in positive_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 1:
                number_of_correct_preds_among_postive_beats += 1
        print("Among %d  %s beats, classifier predicted correctly %d of them" %
              (len(positive_beats_ids), positive_beat, number_of_correct_preds_among_postive_beats))
        print("Accuracy on " + positive_beat + " beats:", number_of_correct_preds_among_postive_beats /
              len(positive_beats_ids))
        print(
            "**********************************************************************************************************"
            "*")

        print( "**************************Results on other Beats:******************************************************"
               "***")
        other_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[0] == 1]
        number_of_correct_preds_among_other_beats = 0
        for j in other_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 0:
                number_of_correct_preds_among_other_beats += 1
        print("Among %d  other beats, classifier predicted correctly %d of them" %
              (len(other_beats_ids), number_of_correct_preds_among_other_beats))
        print("Accuracy on other beats:",
              number_of_correct_preds_among_other_beats / len(other_beats_ids))
        print(
            "**********************************************************************************************************"
            "*")

    print("***********************************************************************************************************")


def evaluate_classifier_with_best_validation(ecg_classifier, test_beats, test_tags, meta_file_name, meta_file_directory,
                                             val_beats=None, val_tags=None, best_val_acc_value=None,
                                             positive_class='TRAIN'):
    """
    :param best_val_acc_value:
    :param val_tags:
    :param val_beats:
    :param ecg_classifier:
    :param test_beats:
    :param test_tags:
    :return:
    """
    print("***********************************************************************************************************")
    print("Evaluating results on test set:")
    accuracy, cost, binary_predictions_of_test_set = ecg_classifier.restore_model_and_eval_acc(test_beats, test_tags,
                                                                                               meta_file_name,
                                                                                               meta_file_directory,
                                                                                               val_beats=val_beats,
                                                                                               val_tags=val_tags,
                                                                                               best_val_acc_value=
                                                                                               best_val_acc_value)

    print("***********************************************************************************")
    print("binary preds first 10:", binary_predictions_of_test_set[:10])
    print("***********************************************************************************")

    if positive_class == 'TRAIN':
        print("**************************Results on Normal Beats:******************************************************"
              "***")
        normal_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[1] == 1]
        number_of_correct_preds_among_normal_beats = 0
        for j in normal_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 1:
                number_of_correct_preds_among_normal_beats += 1
        print("Among %d normal beats, classifier predicted correctly %d of them" %
              (len(normal_beats_ids), number_of_correct_preds_among_normal_beats))
        print("Accuracy on normal beats:", number_of_correct_preds_among_normal_beats / len(normal_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on A Beats:*********************************************************")
        A_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[2] == 1]
        number_of_correct_preds_among_A_beats = 0
        for j in A_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 2:
                number_of_correct_preds_among_A_beats += 1
        print("Among %d A beats, classifier predicted correctly %d of them" %
              (len(A_beats_ids), number_of_correct_preds_among_A_beats))
        print("Accuracy on A beats:", number_of_correct_preds_among_A_beats / len(A_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on V Beats:*********************************************************")
        V_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[3] == 1]
        number_of_correct_preds_among_V_beats = 0
        for j in V_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 3:
                number_of_correct_preds_among_V_beats += 1
        print("Among %d V beats, classifier predicted correctly %d of them" %
              (len(V_beats_ids), number_of_correct_preds_among_V_beats))
        print("Accuracy on V beats:", number_of_correct_preds_among_V_beats / len(V_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on R Beats:*********************************************************")
        R_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[4] == 1]
        number_of_correct_preds_among_R_beats = 0
        for j in R_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 4:
                number_of_correct_preds_among_R_beats += 1
        print("Among %d R beats, classifier predicted correctly %d of them" %
              (len(R_beats_ids), number_of_correct_preds_among_R_beats))
        print("Accuracy on R beats:", number_of_correct_preds_among_R_beats / len(R_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on others Beats:******************************************************"
              "***")
        others_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[0] == 1]
        number_of_correct_preds_among_others_beats = 0
        for j in others_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 0:
                number_of_correct_preds_among_others_beats += 1
        print("Among %d others beats, classifier predicted correctly %d of them" %
              (len(others_beats_ids), number_of_correct_preds_among_others_beats))
        print("Accuracy on others beats:", number_of_correct_preds_among_others_beats / len(others_beats_ids))
        print("********************************************************************************************************"
              "***")

    else:
        positive_beat = positive_class.split('_')[1]
        print("**************************Results on " + positive_beat + " Beats:***************************************"
                                                                        "******************")
        positive_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[1] == 1]
        number_of_correct_preds_among_postive_beats = 0
        for j in positive_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 1:
                number_of_correct_preds_among_postive_beats += 1
        print("Among %d  %s beats, classifier predicted correctly %d of them" %
              (len(positive_beats_ids), positive_beat, number_of_correct_preds_among_postive_beats))
        print("Accuracy on " + positive_beat + " beats:", number_of_correct_preds_among_postive_beats /
              len(positive_beats_ids))
        print(
            "**********************************************************************************************************"
            "*")

        print("**************************Results on other Beats:******************************************************"
               "***")
        other_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[0] == 1]
        number_of_correct_preds_among_other_beats = 0
        for j in other_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 0:
                number_of_correct_preds_among_other_beats += 1
        print("Among %d  other beats, classifier predicted correctly %d of them" %
              (len(other_beats_ids), number_of_correct_preds_among_other_beats))
        print("Accuracy on other beats:",
              number_of_correct_preds_among_other_beats / len(other_beats_ids))
        print(
            "**********************************************************************************************************"
            "*")
    print("***********************************************************************************************************")


def calc_precision_recall(lstm_classifier, x_data, x_tags, meta_file_name=None, save_path=None):
    """

    :param lstm_classifier
    :param x_data:
    :return:
    """
    print("Calculating precision recall on ecg data:")
    probability_predictions = lstm_classifier.get_probability_predictions(x_data, meta_file_name=meta_file_name)
    binary_predictions = lstm_classifier.get_binary_predictions(x_data, meta_file_name=meta_file_name)
    print("***********************************************************************************")
    print("binary preds first 10:", binary_predictions[:10])
    print("***********************************************************************************")
    print("***********************************************************************************")
    print("prob preds first 10:", probability_predictions[:10])
    print("***********************************************************************************")

    print("***********************************************************************************")
    print("truth first 10:", x_tags[:10])
    print("***********************************************************************************")

    lstm_classifier.calc_precision_recall_(x_tags, probability_predictions, save_path=save_path)


def celc_roc_curve(lstm_classifier, x_data, x_tags, save_path, meta_file_name=None, meta_file_directory=None):
    """

    :param lstm_classifier:
    :param x_data:
    :param x_tags:
    :param save_path: where to save the .png file of the ROC curve.
    :return:
    """
    print("Calculating ROC curve on ecg data:")
    probability_predictions = lstm_classifier.get_probability_predictions(x_data, meta_file_name=meta_file_name)
    binary_predictions = lstm_classifier.get_binary_predictions(x_data, meta_file_name=meta_file_name)
    print("***********************************************************************************")
    print("binary preds first 10:", binary_predictions[:10])
    print("***********************************************************************************")
    print("***********************************************************************************")
    print("prob preds first 10:", probability_predictions[:10])
    print("***********************************************************************************")

    print("***********************************************************************************")
    print("truth first 10:", x_tags[:10])
    print("***********************************************************************************")
    lstm_classifier.calc_roc(x_tags, probability_predictions, save_path)


def add_data_from_gan(gan_meta_file,
                      number_samples_to_generate, type_of_beat_generated, length_of_each_sample, number_of_classes,
                      is_conditional_gan=False, label_index=None):
    """

    :param gan_meta_file:
    :param meta_file_directory:
    :param number_samples_to_generate:
    :param type_of_beat_generated:
    :param length_of_each_sample:
    :return:
    """
    beats = generator.generate_data_from_trained_generator(number_samples_to_generate, gan_meta_file,
                                                           is_conditional_gan, label_index)
    if number_of_classes == 5:
        if type_of_beat_generated == 'R':
            tags = np.array([4 for _ in range(len(beats))])
        elif type_of_beat_generated == "A":
            tags = np.array([2 for _ in range(len(beats))])
        elif type_of_beat_generated == "V":
            tags = np.array([3 for _ in range(len(beats))])
        elif type_of_beat_generated == "N":
            tags = np.array([1 for _ in range(len(beats))])
        else:
            pass
            # TODO: ADD suuport to more labels

    else:
        tags = np.array([1 for _ in range(len(beats))])
    # 2. Set each beat to fit as an entrance to the LSTM:
    lstm_beats = [[beat[i:i + length_of_each_sample] for i in range(0, len(beat), length_of_each_sample)] for
                        beat
                        in beats]

    dataset = generic_dataset.GenericDataSetIterator(beats, tags)
    tags = dataset.convert_labels_to_one_hot_vector(number_of_classes)
    return lstm_beats, tags


def add_random_noise(number_samples_to_generate, length_of_each_sample):
    """

    :param number_samples_to_generate:
    :param length_of_each_sample:
    :return:
    """
    beats = randomize_beats(number_samples_to_generate)
    tags = np.array([1 for _ in range(len(beats))])
    lstm_beats = [[beat[i:i + length_of_each_sample] for i in range(0, len(beat), length_of_each_sample)] for
                  beat
                  in beats]
    dataset = generic_dataset.GenericDataSetIterator(beats, tags)
    tags = dataset.convert_labels_to_one_hot_vector(2)
    return lstm_beats, tags


def randomize_beats(number_samples_to_generate):
    """

    :param number_samples_to_generate:
    :return:
    """
    x = np.random.normal(-1, 1, [number_samples_to_generate, 240])
    y = x.reshape((number_samples_to_generate, 1, 240, 1))
    return y

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    # parser.add_argument('--classifier_architecture', type=str, help='Which type of network to use',
    #                    choices=['LSTM', 'FC'])
    parser.add_argument('--database_type', type=str, help='which way to divide the data', choices=['AAMI', 'REGULAR'],
                        default='REGULAR')
    parser.add_argument('--tensor_board_dir', type=str, default='./logs', help='directory to save log events')
    parser.add_argument('--saved_models_dir', type=str, default='./saved_models', help='directory to save model values')
    parser.add_argument('--beat_to_classify', type=str, default='ALL',
                        help='which beat to classify against all others. '
                             'if All is chosen then a multi class classifier will be trained', choices=
                        ['A', 'R', 'V', 'N', 'O', 'ALL', 'S', 'Q', 'F'])

    parser.add_argument('--use_gan', action='store_true', help='Use a generator and add synthetic data')
    parser.add_argument('--gan_type', type=str, default='A', help='Type of ECG class to generate',
                        choices=['A', 'R', 'V', 'N', 'O', 'Q', 'S', 'F', 'DC_CONDITIONAL'])
    parser.add_argument('--gan_meta_file', type=str, help='The meta file of the GAN')
    parser.add_argument('--gan_meta_file_directory', type=str, help='The directory of the meta file of the gan')
    parser.add_argument('--num_of_examples_to_add_from_gan', default=0, type=int,
                        help='How many examples to generate from the GAN')

    parser.add_argument('--mode', type=str, help='Which mode to apply the LSTM',
                        choices=['TRAIN', 'TEST', 'TRAIN_V', 'TRAIN_R', 'TRAIN_A', 'TRAIN_N', 'TRAIN_O', 'TRAIN_S',
                                 'TRAIN_Q', 'TRAIN_F'],
                        default='TRAIN')
    parser.add_argument('--best_acc_validation_value', type=float, help='what was the best accuracy on the validation '
                                                                        'set when training')
    parser.add_argument('--loaded_graph_name', type=str, help='Name of the meta file of the trained LSTM')
    parser.add_argument('--meta_file_directory', type=str, help="Path to the directory of the meta file")

    parser.add_argument('--save_data_to_pickle', action='store_true', help='Save dataset into a pickle file')
    parser.add_argument('--pickle_path', type=str, help='pickle path to save or load the dataset', default=None)
    parser.add_argument('--use_data_from_pickle', action='store_true', help='Load dataset from oickle file')

    parser.add_argument('--results_dir', type=str, default='./results', help='directory to save roc curve and pre-rec')

    args = parser.parse_args()

    print("Database type: ", args.database_type)
    print("Save model dir:", args.saved_models_dir)
    print("Logs dir:", args.tensor_board_dir)
    print("Use Gan ? : ", args.use_gan)
    print("Number of examples to use from GAN:", args.num_of_examples_to_add_from_gan)
    print("The roc curve file will be saved at:", os.path.join(
                'aami', args.beat_to_classify, 'roc_curve_' + str(args.num_of_examples_to_add_from_gan))
          + '_auto_script.png')
    if args.use_gan:
        print("GAN type", args.gan_type)

    if args.use_data_from_pickle:
        lstm_beats_train, train_tags, lstm_beats_val, validation_tags, lstm_beats_test, test_tags = \
            load_dataset_from_pickle(args.pickle_path)
    else:
        if args.database_type == 'REGULAR':
            if args.beat_to_classify == 'ALL':
                lstm_beats_train, train_tags, lstm_beats_val, validation_tags, lstm_beats_test, test_tags = create_dataset(
                    os.path.join('Data', 'MIT database'), number_of_classes=5, length_of_each_sample=5,
                    save_to_pickle=args.save_data_to_pickle, pickle_path=args.pickle_path, positive_class=None)

            else:
                lstm_beats_train, train_tags, lstm_beats_val, validation_tags, lstm_beats_test, test_tags = create_dataset(
                    os.path.join('Data', 'MIT database'), number_of_classes=2, length_of_each_sample=5,
                    save_to_pickle=args.save_data_to_pickle,
                    pickle_path=args.pickle_path, positive_class=args.beat_to_classify)

        else:
            assert args.database_type == 'AAMI'
            lstm_beats_train, train_tags, lstm_beats_val, validation_tags, lstm_beats_test, test_tags = \
                create_dataset_according_to_aami(positive_class=args.beat_to_classify,
                                                 length_of_each_sample=5, save_to_pickle=args.save_data_to_pickle,
                                                 pickle_path=args.pickle_path, print_statistics=False,
                                                 drop_other_beats=True)
            # lstm_beats_train = np.concatenate((lstm_beats_train, lstm_beats_val), axis=0)
            # assert lstm_beats_train.shape[1] == 240
            # train_tags = np.concatenate((train_tags, validation_tags), axis=0)
            # assert train_tags.shape[1] == 2

    if args.use_gan:
        print("Adding synthetic %s beats:" % args.gan_type)
        if args.mode == 'TRAIN':
            number_of_classes = 5
        else:
            number_of_classes = 2

        if args.gan_type != "DC_CONDITIONAL":
            gan_beats, gan_tags = add_data_from_gan(args.gan_meta_file,
                                                    number_samples_to_generate=args.num_of_examples_to_add_from_gan,
                                                    type_of_beat_generated=args.gan_type, length_of_each_sample=5,
                                                    number_of_classes=number_of_classes)
        else:
            if args.beat_to_classify == 'N':
                y_index = 0
            elif args.beat_to_classify == 'S':
                y_index = 1
            elif args.beat_to_classify == 'V':
                y_index = 2
            elif args.beat_to_classify == 'F':
                y_index = 3
            else:
                assert args.beat_to_classify == 'Q'
                y_index = 4
            gan_beats, gan_tags = add_data_from_gan(args.gan_meta_file,
                                                    number_samples_to_generate=args.num_of_examples_to_add_from_gan,
                                                    type_of_beat_generated=args.gan_type, length_of_each_sample=5,
                                                    number_of_classes=number_of_classes, is_conditional_gan=True,
                                                    label_index=y_index)

        lstm_beats_train = np.concatenate((lstm_beats_train, gan_beats), axis=0)
        train_tags = np.concatenate((train_tags, gan_tags), axis=0)

        # Don't need the gan graph now, so clean it:
        tf.reset_default_graph()

    if args.mode == 'TRAIN' or args.mode == 'TEST':
        ecg_lstm = create_lstm_classifier(number_of_classes=5, sequence_length=48, length_of_each_sample=5,
                                          tensor_board_dir=args.tensor_board_dir,
                                          save_model_dir=args.saved_models_dir,
                                          dropout_prob=1, mode=args.mode)
    else:
        ecg_lstm = create_lstm_classifier(number_of_classes=2, sequence_length=48, length_of_each_sample=5,
                                          tensor_board_dir=args.tensor_board_dir,
                                          save_model_dir=args.saved_models_dir,
                                          dropout_prob=1, mode=args.mode)

    if args.mode in ['TRAIN', 'TRAIN_V', 'TRAIN_R', 'TRAIN_A', 'TRAIN_N', 'TRAIN_O', 'TRAIN_Q', 'TRAIN_F', 'TRAIN_S']:
        train(ecg_lstm, train_beats=lstm_beats_train, train_tags=train_tags, validation_beats=lstm_beats_val,
              validation_tags=validation_tags, number_of_iterations=10000, batch_size=16)

        evaluate_classifier(ecg_classifier=ecg_lstm, test_beats=lstm_beats_test, test_tags=test_tags,
                            positive_class=args.mode)

        if args.mode in ['TRAIN_V', 'TRAIN_R', 'TRAIN_A', 'TRAIN_N', 'TRAIN_O', 'TRAIN_Q', 'TRAIN_F', 'TRAIN_S']:
            calc_precision_recall(ecg_lstm, lstm_beats_test, test_tags, meta_file_name=None,
                                  save_path=os.path.join(
                args.results_dir, 'roc_curve_' + str(args.num_of_examples_to_add_from_gan))
                                                                           + '_auto_script.png')

            celc_roc_curve(ecg_lstm, lstm_beats_test, test_tags, save_path=os.path.join(
                args.results_dir, 'roc_curve_' + str(args.num_of_examples_to_add_from_gan))
                                                                           + '_auto_script.png')

    elif args.mode == "TEST":

        evaluate_classifier_with_best_validation(ecg_lstm, lstm_beats_test, test_tags, args.loaded_graph_name,
                                                 meta_file_directory=args.meta_file_directory,
                                                 val_beats=lstm_beats_val, val_tags=validation_tags,
                                                 best_val_acc_value=args.best_acc_validation_value,
                                                 positive_class='TRAIN_V')

        calc_precision_recall(ecg_lstm, lstm_beats_test, test_tags, meta_file_name=args.loaded_graph_name,
                              meta_file_directory=args.meta_file_directory)

        celc_roc_curve(ecg_lstm, lstm_beats_test, test_tags, meta_file_name=args.loaded_graph_name,
                       meta_file_directory=args.meta_file_directory, save_path=
                       os.path.join('results', args.beat_to_classify, 'roc_curve_' +
                                    str(args.num_of_examples_to_add_from_gan)) + '_early_stop_auto_script.png')

    # print("DONE")
