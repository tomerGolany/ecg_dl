from ecg_dl import read_data
import numpy as np
import os
from my_dl_lib.generic_gan import gan
import argparse


def create_ecg_dcgan(z_length, beat_length, tensor_board_logs_dir, saved_model_dir, generated_samples_dir):
    """
    Create a DCGAN which has an architecture to generate ecg beats.
    :param z_length: how many numbers to randomize
    :param beat_length: length of one beat, i.e length of one sample
    :param tensor_board_logs_dir:
    :param saved_model_dir:
    :param generated_samples_dir:
    :return: generic gan instance.
    """
    print("Initializing ECG GAN:")
    ecg_gan = gan.GenericGAN(discriminator_input_height=beat_length, discriminator_input_width=1,
                             discriminator_number_of_input_channels=1,
                             discriminator_number_of_filters_at_first_layer=64,
                             discriminator_arch_file=None, generator_input_dim=z_length,
                             generator_output_height=beat_length,
                             generator_output_width=1, generator_output_channels=1, generator_architecture_file=None,
                             tensor_board_logs_dir=os.path.join(tensor_board_logs_dir),
                             is_input_an_image=False,
                             checkpoint_dir=os.path.join(saved_model_dir),
                             generated_samples_dir=os.path.join(generated_samples_dir), is_conditional_gan=True,
                             y_labels_dim=5)

    print("ECG GAN create successfully")
    return ecg_gan


def create_dataset_according_to_aami(type_of_beat_to_generate, conditional_gan=False):
    """

    :param type_of_beat_to_generate:
    :return:
    """
    train_set, validation_set, test_set = read_data.divide_data_to_train_test_according_to_aami()

    train_set = train_set + validation_set
    if not conditional_gan:
        # To avoid overfitting, try to generate only from the train and val set. We dont really care about the tags
        # generation.
        train_beats, train_tags = read_data.merge_data_of_patients(train_set, binary_pred=True,
                                                                   postivie_class=type_of_beat_to_generate,
                                                                   aami_standarts=True)
    else:
        train_beats, train_tags = read_data.merge_data_of_patients(train_set, binary_pred=False,
                                                                   postivie_class=type_of_beat_to_generate,
                                                                   aami_standarts=True)

    if not conditional_gan:
        filterd_beats = [x for i, x in enumerate(train_beats) if train_tags[i][0] == '1']
        filterd_tags = [x for x in train_tags if x[0] == '1']
        assert len(filterd_beats) == len(filterd_tags)
        for tag in filterd_tags:
            assert tag[0] == '1'
        print("Found %d original beats of type %s to be trained" % (len(filterd_beats), type_of_beat_to_generate))
        filterd_tags = np.array([int(x[0]) for x in filterd_tags])

        return np.array(filterd_beats), np.array(filterd_tags)
    else:
        one_hot_tags = []
        for tag in train_tags:
            one_hot = [0 for x in range(5)]
            one_hot[tag] = 1
            one_hot_tags.append(one_hot)

        train_tags = np.array(one_hot_tags)
        print("Total number of beats to train on GAN: ", len(train_beats))
        return train_beats, train_tags


def create_dataset(type_of_beat_to_generate, original_data_dir):
    """

    :param type_of_beat_to_generate:
    :param original_data_dir:
    :return:
    """
    patient_numbers = read_data.get_list_of_all_patient_numbers(original_data_dir)
    train_set, validation_set, test_set = read_data.divide_data_to_train_val_test(patient_numbers)

    # To avoid overfitting, try to generate only from the train set.
    train_beats, train_tags = read_data.merge_data_of_patients(train_set, binary_pred=False)

    if type_of_beat_to_generate == 'N':
        class_number_to_generate = 1
    elif type_of_beat_to_generate == 'A':
        class_number_to_generate = 2
    elif type_of_beat_to_generate == 'V':
        class_number_to_generate = 3
    elif type_of_beat_to_generate == 'R':
        class_number_to_generate = 4
    elif type_of_beat_to_generate == 'O':
        class_number_to_generate = 0
    else:
        print("Invalid beat type.. Exiting.")
        return -1

    filterd_beats, filterd_tags = read_data.get_all_beats_of_type(class_number_to_generate, train_beats, train_tags)
    assert len(filterd_beats) == len(filterd_tags)
    print("Found %d original beats of type %s to be trained" % (len(filterd_beats), type_of_beat_to_generate))
    # Just for debugging:
    for t in filterd_tags:
        assert t == class_number_to_generate

    return filterd_beats, filterd_tags


def train_dc_gan(ecg_gan, real_beats, real_tags, number_of_iterations, batch_size):
    """

    :param ecg_gan:
    :param real_beats:
    :param real_tags:
    :param number_of_iterations:
    :param batch_size:
    :return:
    """
    print("*******************************Starting to train ECG GAN************************************")
    ecg_gan.train(discriminator_x_train=real_beats, discriminator_y_train=real_tags,
                  num_of_iterations=number_of_iterations, batch_size=batch_size, learning_rate=0.0002,
                  momentum_term_adam=0.5)
    print("*******************************Train Complete***********************************************")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--database_type', type=str, help='which way to divide the data', choices=['AAMI', 'REGULAR'],
                        default='REGULAR')
    parser.add_argument('--tensor_board_dir', type=str, help='directory to save log events')
    parser.add_argument('--saved_models_dir', type=str, help='directory to save model values')
    parser.add_argument('--generated_samples_dir', type=str, help='directory to save samples from generator')

    parser.add_argument('--gan_type', type=str, default='A', help='Type of ECG class to generate',
                        choices=['A', 'R', 'V', 'N', 'O', 'S', 'F', 'Q'])
    parser.add_argument('--conditional_gan',  action='store_true', help='Use a conditional GAN?')
    args = parser.parse_args()
    print("Use database type: ", args.database_type)
    print("Train GAN of %s beats", args.gan_type)

    if args.database_type == 'REGULAR':
        filterd_beats, filterd_tags = create_dataset(type_of_beat_to_generate=args.gan_type,
                                                     original_data_dir=os.path.join('Data', 'MIT database'))
    else:
        assert args.database_type == 'AAMI'
        filterd_beats, filterd_tags = create_dataset_according_to_aami(type_of_beat_to_generate=args.gan_type,
                                                                       conditional_gan=args.conditional_gan)

    ecg_dcgan = create_ecg_dcgan(100, 240, tensor_board_logs_dir=args.tensor_board_dir,
                                 saved_model_dir=args.saved_models_dir, generated_samples_dir=args.generated_samples_dir)

    train_dc_gan(ecg_dcgan, filterd_beats, filterd_tags, number_of_iterations=50000, batch_size=64)
