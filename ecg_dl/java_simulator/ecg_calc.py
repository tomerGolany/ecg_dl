import math
import numpy as np
import ecg_params

class EcgCalc:

    def __init__(self, ecg_parameters, ecg_log_window = None):
        """
        Creates a new instance of EcgCalc
        :param ecg_parameters: ecg parameters instance .
        :param ecg_log_window: we will try to avid this because it comes from the GUI, and we dont care about the GUI
        """

        self.rk4_count = 0
        self.count = 0
        self.count_run1 = 0
        # Init constants : ##########################
        self.PI = math.pi
        self.NR_END = 1
        self.IA = 16807
        self.IM = 2147483647
        self.AM = (1.0 / self.IM)
        self.IQ = 127773
        self.IR = 2836
        self.NTAB = 32
        self.NDIV = (1 + (self.IM - 1) / self.NTAB)
        self.EPS = 1.2e-7
        self.RNMX = (1.0 - self.EPS)
        ############################################

        # Init variables :
        # Order of extrema: [P Q R S T]:
        self.theta_i = [0 for x in range(6)]  # new calculated theta converted to radians
        self.a_i = [0 for x in range(6)]  # new calculated a
        self.b_i = [0 for x in range(6)]  # # new calculated b

        self.num_of_ecg_outputs = 0
        self.sys_state_space_dims = 3
        self.x_initial_value = 1.0
        self.y_initial_vale = 0.0
        self.z_initial_value = 0.04

        self.rseed = None  # Long type
        self.h = None
        self.rr = None
        self. rrpc = None

        # ECG result variables :
        # result vectors :
        self.ecg_result_time = None
        self.ecg_result_voltage = None
        self.ecg_result_peaks = None

        self.ecg_result_num_rows = None

        self.ecg_params = ecg_parameters
        self.ecg_logs = ecg_log_window

        # variables for the static function rang()
        self.iy = 0
        self.iv = [0 for x in range(32)]  # later we will fill this with elements of type Long ?.

    def calculate_ecg(self):
        """

        :return:
        """
        ret_value = True
        # print("Starting to calculate ECG.....")

        ret_value = self.run_ecg_calcs()

        # print("Finished calculating ECG table data")

        return ret_value

    def ran1(self):
        """
        TODO: Give a better name
        :return:
        """
        if self.iy == 0:
            flg = False
        else:
            flg = True

        if self.rseed <= 0 or not flg:
            if -1 * self.rseed < 1:
                self.rseed = 1
            else:
                self.rseed = -1 * self.rseed

            j = self.NTAB + 7

            while j >= 0:
                k = int((self.rseed) / (self.IQ))
                self.rseed = self.IA * (self.rseed - k * self.IQ) - self.IR * k
                if self.rseed < 0:
                    self.rseed += self.IM
                if j < self.NTAB:
                    self.iv[j] = self.rseed
                j -= 1
            self.iy = self.iv[0]


        k = int((self.rseed) / (self.IQ))
        self.rseed = self.IA * ( self.rseed - k * self.IQ) - self.IR * k

        if self.rseed < 0:
            self.rseed += self.IM

        j = int(float(self.iy)/ float(self.NDIV))
        self.iy = self.iv[j]
        self.iv[j] = self.rseed

        if (self.AM * self.iy) > self.RNMX:
            return self.RNMX
        else:
            return self.AM * self.iy

    def ifft(self, data, nn, isign):
        """

        :param data:
        :param nn:
        :param isign:
        :return:
        """
        n = nn * 2
        j = 1
        i = 1
        # print("length of data: ", len(data))
        # print("Value of N", n)
        while i < n:
            if j > i:
                # print("j: ", j)
                swap = data[int(j)]
                data[int(j)] = data[int(i)]
                data[int(i)] = swap

                swap = data[int(j) + 1]
                data[int(j) + 1] = data[int(i) + 1]
                data[int(i) + 1] = swap
            m = n >> 1
            if j == 1:
                # print("m:", m)
                pass
            while m >= 2 and j > m:
                j -= m
                m >>= 1
            j += m

            i += 2

        mmax = 2

        while n > mmax:
            istep = mmax * 2
            theta = isign * (6.28318530717959/float(mmax))
            wtemp = math.sin(0.5 * theta)
            wpr = -2.0 * wtemp * wtemp
            wpi = math.sin(theta)
            wr = 1.0
            wi = 0.0

            m = 1
            while m < mmax:
                i = m
                while i <= n:
                    j = i + mmax
                    tempr = wr * data[int(j)] - wi * data[int(j)+ 1]
                    tempi = wr * data[int(j) + 1] + wi * data[int(j)]

                    data[int(j)] = data[int(i)] - tempr
                    data[int(j) + 1] = data[int(i) + 1] - tempi
                    data[int(i)] += tempr
                    data[int(i) + 1] += tempi

                    i += istep
                wtemp = wr
                wr = wtemp * wpr - wi * wpi + wr
                wi = wi * wpr + wtemp * wpi + wi

                m += 2
            mmax = istep

    # TODO: CHECK WHY is starts from 1 and not zero the index .,...
    def stdev_calc(self, x, n):
        """

        :param x:
        :param n:
        :return:
        """
        add = 0.0
        j = 1
        while j <= n:
            add += x[j]
            j += 1
        mean = float(add) / float(n)

        total = 0.0
        for j in range(1, n + 1):
            diff = x[j] - mean
            total += diff * diff

        return math.sqrt(float(total) / float(n - 1))

    def ang_freq_calc(self, t):
        """
        THE ANGULAR FREQUENCY
        :param t:
        :return:
        """

        i = 1 + int(math.floor(t / self.h))

        if self.rrpc[i] == 0:
            return math.inf
        return 2.0 * self.PI / float(self.rrpc[i])

    def derivs_pqrst_calc(self, t0, x, dxdt):
        """
        THE EXACT NONLINEAR DERIVATIVES
        :param t0:
        :param x:
        :param dxdt:
        :return:
        """

        k = 5
        xi = [0 for e in range(k+1)]  # double[k + 1];
        yi = [0 for e in range(k+1)]  # new double[k + 1];
        w0 = self.ang_freq_calc(t0)
        r0 = 1.0
        x0 = 0.0
        y0 = 0.0
        z0 = 0.0
        a0 = 1.0 - math.sqrt((x[1] - x0) * (x[1] - x0) + (x[2] - y0) * (x[2] - y0)) / r0

        for i in range(1, k + 1):
            xi[i] = math.cos(self.theta_i[i])
            yi[i] = math.sin(self.theta_i[i])

        zbase = 0.005 * math.sin(2.0 * self.PI * self.ecg_params.getFHi() * t0)

        t = math.atan2(x[2], x[1])
        dxdt[1] = a0 * (x[1] - x0) - w0 * (x[2] - y0)
        dxdt[2] = a0 * (x[2] - y0) + w0 * (x[1] - x0)

        dxdt[3] = 0.0

        for i in range(1, k+1):
            # dt = (t - self.theta_i[i]) % (2.0 * self.PI)
            dt = math.fmod(t - self.theta_i[i], 2.0 * self.PI)
            dt2 = dt * dt
            if self.b_i[i] == 0.0:
                dxdt[3] += math.inf
            else:
                dxdt[3] += -1*self.a_i[i] * dt * math.exp(-0.5 * dt2 / (self.b_i[i] * self.b_i[i]))

        dxdt[3] += -1.0 * (x[3] - zbase)

    def RK4(self, y, n, x, h, yout):
        """
        RUNGA-KUTTA FOURTH ORDER INTEGRATION
        :param y:
        :param n:
        :param x:
        :param h:
        :param yout:
        :return:
        """
        self.rk4_count += 1

        dydx = [0 for e in range(0, n + 1)]  # double[n + 1];
        dym = [0 for e in range(0, n + 1)]  # double[n + 1];
        dyt = [0 for e in range(0, n + 1)]  # double[n + 1];
        yt = [0 for e in range(0, n + 1)]  # double[n + 1];

        hh = h * 0.5
        h6 = h / 6.0
        xh = x + hh

        self.derivs_pqrst_calc(x, y, dydx)

        for i in range(1, n + 1):
            yt[i] = y[i] + hh * dydx[i]

        self.derivs_pqrst_calc(xh, yt, dyt)
        for i in range(1, n + 1):
            yt[i] = y[i] + hh * dyt[i]

        self.derivs_pqrst_calc(xh, yt, dym)
        for i in range(1, n + 1):
            yt[i] = y[i] + h * dym[i]
            dym[i] += dyt[i]

        self.derivs_pqrst_calc(x + h, yt, dyt)
        for i in range(1, n + 1):
            yout[i] = y[i] + h6 * (dydx[i] + dyt[i] + 2.0 * dym[i])

    def rrprocess(self, rr, flo, fhi, flostd, fhistd, lfhfratio, hrmean, hrstd, sf, n):
        """
        GENERATE RR PROCESS
        :param rr:
        :param flo:
        :param fhi:
        :param flostd:
        :param fhistd:
        :param lfhfratio:
        :param hrmean:
        :param hrstd:
        :param sf:
        :param n:
        :return:
        """

        w = [0 for e in range(n+1)]  # new double[n + 1];
        Hw = [0 for e in range(n+1)]
        Sw = [0 for e in range(n+1)]
        ph0_len = int(n / 2.0 - 1 + 1)
        ph0 = [0 for e in range(ph0_len)]  # new double[(int)(n / 2 - 1 + 1)];
        ph = [0 for e in range(n+1)]
        SwC = [0 for e in range(2 *n + 1)]  # new double[(2 * n) + 1];

        w1 = 2.0 * self.PI * flo
        w2 = 2.0 * self.PI * fhi
        c1 = 2.0 * self.PI * flostd
        c2 = 2.0 * self.PI * fhistd
        sig2 = 1.0
        sig1 = lfhfratio
        rrmean = 60.0 / hrmean
        rrstd = 60.0 * hrstd / (hrmean * hrmean)

        df = sf / float(n)

        for i in range(1, n+1):
            w[i] = (i - 1) * 2.0 * self.PI * df

        for i in range(1, n + 1):
            Hw[i] = (sig1 * math.exp(-0.5 * (math.pow(w[i] - w1, 2) / math.pow(c1, 2))) /
                     math.sqrt(2 * self.PI * c1 * c1)) + \
                    (sig2 * math.exp(-0.5 * (math.pow(w[i]-w2, 2)/math.pow(c2, 2))) / math.sqrt(2 * self.PI*c2*c2))

        for i in range(1, int(n/2) + 1):
            Sw[i] = (sf / 2.0) * math.sqrt(Hw[i])

        for i in range(int(n/2) + 1, n + 1):
            Sw[i] = (sf / 2.0) * math.sqrt(Hw[n - i + 1])

        # randomise the phases :
        for i in range(1, int(n/2)):
            ph0[i] = 2.0 * self.PI * self.ran1()

        ph[1] = 0.0
        for i in range(1, int(n/2)):
            ph[i + 1] = ph0[i]

        ph[int(n / 2) + 1] = 0.0
        for i in range(1, int(n/2)):
            ph[n - i + 1] = - ph0[i]


        # make complex spectrum :
        for i in range(1, n + 1):
            SwC[2 * i - 1] = Sw[i] * math.cos(ph[i])

        for i in range(1, n + 1):
            SwC[2 * i] = Sw[i] * math.sin(ph[i])


        # Calclate inverse fft:
        self.ifft(SwC, n, -1)


        # Excecute real part :
        for i in range(1, n + 1):
            rr[i] = (1.0 / float(n))*SwC[2 * i - 1]

        xstd = self.stdev_calc(rr, n)
        ratio = rrstd / xstd

        # print(xstd)
        # print(ratio)

        for i in range(1, n + 1):
            rr[i] *= ratio

        for i in range(1, n + 1):
            rr[i] += rrmean

        '''
        print("rr 10")
        for q in range(10):
            print(rr[q])
        '''

    def detect_peaks(self, ipeak, x, y, z, n):
        """

        :param ipeak:
        :param x:
        :param y:
        :param z:
        :param n:
        :return:
        """
        thetap1 = self.theta_i[1]
        thetap2 = self.theta_i[2]
        thetap3 = self.theta_i[3]
        thetap4 = self.theta_i[4]
        thetap5 = self.theta_i[5]

        for i in range(1, n+1):
            ipeak[i] = 0.0

        theta1 = math.atan2(y[1], x[1])

        for i in range(1, n):
            theta2 = math.atan2(y[i + 1], x[i + 1])

            if theta1 <= thetap1 and thetap1 <= theta2 :
                d1 = thetap1 - theta1
                d2 = theta2 - thetap1
                if d1 < d2 :
                    ipeak[i] = 1.0
                else:
                    ipeak[i+1] = 1.0

            elif theta1 <= thetap2 and thetap2 <= theta2:
                d1 = thetap2 - theta1
                d2 = theta2 - thetap2
                if d1 < d2:
                    ipeak[i] = 2.0
                else:
                    ipeak[i + 1] = 2.0

            elif theta1 <= thetap3 and thetap3 <= theta2:
                d1 = thetap3 - theta1
                d2 = theta2 - thetap3

                if d1 < d2:
                    ipeak[i] = 3.0
                else:
                    ipeak[i + 1] = 3.0

            elif theta1 <= thetap4 and thetap4 <= theta2:
                d1 = thetap4 - theta1
                d2 = theta2 - thetap4
                if d1 < d2:
                    ipeak[i] = 4.0
                else:
                    ipeak[i+1] = 4.0

            elif theta1 <= thetap5 and thetap5 <= theta2:
                d1 = thetap5 - theta1
                d2 = theta2 - thetap5
                if d1 < d2:
                    ipeak[i] = 5.0
                else:
                    ipeak[i+1] = 5.0

            theta1 = theta2

        # Correct the peaks:
        d = int(math.ceil(self.ecg_params.getSfEcg() / 64))
        for i in range(1, n + 1):
            if ipeak[i] == 1 or ipeak[i] == 3 or ipeak[i] == 5:

                if 1 > (i - d):
                    j1 = 1
                else:
                    j1 = i - d  # MAX(1, i - d)

                if n < (i + d):
                    j2 = n
                else:
                    j2 = i + d  # MIN(n, i + d)

                jmax = j1
                zmax = z[j1]
                for j in range(j1 + 1, j2 + 1):
                    if z[j] > zmax:
                        jmax = j
                        zmax = z[j]

                if jmax != i:
                    ipeak[jmax] = ipeak[i]
                    ipeak[i] = 0

            elif ipeak[i] == 2 or ipeak[i] == 4:
                if 1 > i - d:
                    j1 = 1
                else:
                    j1 = i - d  # MAX(1,i-d)

                if n < i + d:
                    j2 = n
                else:
                    j2 = i + d  # MIN(n,i+d)

                jmin = j1
                zmin = z[j1]

                for j in range(j1 + 1, j2 + 1):
                    if z[j] < zmin:
                        jmin = j
                        zmin = z[j]

                if jmin != i:
                    ipeak[jmin] = ipeak[i]
                    ipeak[i] = 0

    def run_ecg_calcs(self):
        """
        Run part of program.
        :return:
        """
        RetValue = True

        # Perform some checks on input values:
        q = int(np.rint(self.ecg_params.getSf() / self.ecg_params.getSfEcg()))
        qd = float(self.ecg_params.getSf()) / float(self.ecg_params.getSfEcg())

        # Convert angles from degrees to radians and copy a vector to ai:
        for i in range(1, 6):
            self.theta_i[i] = self.ecg_params.getTheta(i - 1) * self.PI / 180.0
            self.a_i[i] = self.ecg_params.getA(i - 1)

        # Adjust extrema parameters for mean heart rate:
        hrfact = math.sqrt(self.ecg_params.getHrMean() / 60)
        hrfact2 = math.sqrt(hrfact)

        for i in range(1, 6):
            self.b_i[i] = self.ecg_params.getB(i - 1) * hrfact

        self.theta_i[1] *= hrfact2
        self.theta_i[2] *= hrfact
        self.theta_i[3] *= 1.0
        self.theta_i[4] *= hrfact
        self.theta_i[5] *= 1.0

        # Declare state vector:
        x = [0 for e in range(4)] # new double[4]
        '''
        print("Approximate number of heart beats: " + str(self.ecg_params.getN()))
        print("ECG sampling frequency: " + str(self.ecg_params.getSfEcg()) + " Hertz")
        print("Internal sampling frequency: " + str(self.ecg_params.getSf()) + " Hertz")
        print("Amplitude of additive uniformly distributed noise: " + str(self.ecg_params.getANoise()) + " mV")
        print("Heart rate mean: " + str(self.ecg_params.getHrMean()) + " beats per minute")
        print("Heart rate std: " + str(self.ecg_params.getHrStd()) + " beats per minute")
        print("Low frequency: " + str(self.ecg_params.getFLo()) + " Hertz")
        print("High frequency std: " + str(self.ecg_params.getFHiStd()) + " Hertz")
        print("Low frequency std: " + str(self.ecg_params.getFLoStd()) + " Hertz")
        print("High frequency: " + str(self.ecg_params.getFHi()) + " Hertz")
        print("LF/HF ratio: " + str(self.ecg_params.getLfHfRatio()))
        print("time step milliseconds: " + str(self.ecg_params.getEcgAnimateInterval()) + "\n")
        print("Order of Extrema:")
        print("      theta(radians)")
        print("P: [" + str(self.theta_i[1]) + "\t]")
        print("Q: [" + str(self.theta_i[2]) + "\t]")
        print("R: [" + str(self.theta_i[3]) + "\t]")
        print("S: [" + str(self.theta_i[4]) + "\t]")
        print("T: [" + str(self.theta_i[5]) + "\t]\n")
        print("      a(calculated)")
        print("P: [" + str(self.a_i[1]) + "\t]")
        print("Q: [" + str(self.a_i[2]) + "\t]")
        print("R: [" + str(self.a_i[3]) + "\t]")
        print("S: [" + str(self.a_i[4]) + "\t]")
        print("T: [" + str(self.a_i[5]) + "\t]\n")
        print("      b(calculated)")
        print("P: [" + str(self.b_i[1]) + "\t]")
        print("Q: [" + str(self.b_i[2]) + "\t]")
        print("R: [" + str(self.b_i[3]) + "\t]")
        print("S: [" + str(self.b_i[4]) + "\t]")
        print("T: [" + str(self.b_i[5]) + "\t]\n")
        '''
        # Initialise the vector
        x[1] = self.x_initial_value
        x[2] = self.y_initial_vale
        x[3] = self.z_initial_value

        # Initialise seed:
        self.rseed = -self.ecg_params.getSeed()

        # Calculate time scales:
        self.h = 1.0 / float(self.ecg_params.getSf())
        tstep = 1.0 / float(self.ecg_params.getSfEcg())

        # Calculate length of RR time series:
        rrmean = (60.0 / self.ecg_params.getHrMean())
        Nrr = int(math.pow(2.0, math.ceil(math.log(self.ecg_params.getN() * rrmean * self.ecg_params.getSf()) /
                                            math.log(2.0))))

        # print("Using " + str(Nrr) + " = 2^ " + str(int(math.log(1.0 * Nrr) / math.log(2.0))) +
        #       " samples for calculating RR intervals")

        # Create rrprocess with required spectrum:
        self.rr = [0 for e in range(Nrr + 1)]  # new double[Nrr + 1]

        self.rrprocess(self.rr, self.ecg_params.getFLo(), self.ecg_params.getFHi(), self.ecg_params.getFLoStd(),
                  self.ecg_params.getFHiStd(), self.ecg_params.getLfHfRatio(), self.ecg_params.getHrMean(),
                  self.ecg_params.getHrStd(), self.ecg_params.getSf(), Nrr)

        # Create piecewise constant rr:
        self.rrpc = [0 for e in range((2 * Nrr) + 1)]  # new double[(2 * Nrr) + 1];
        tecg = 0.0
        i = 1
        j = 1
        while i <= Nrr:
            tecg += self.rr[j]
            j = int(np.rint(tecg / self.h))

            for k in range(i, j + 1):
                self.rrpc[k] = self.rr[i]
            i = j+1

        Nt = j
        # print(Nt)
        # Integrate dynamical system using fourth order Runge-Kutta:
        xt = [0 for e in range(Nt + 1)] # new double[Nt + 1];
        yt = [0 for e in range(Nt + 1)]  # new double[Nt + 1];
        zt = [0 for e in range(Nt + 1)]  # new double[Nt + 1];
        timev = 0.0

        for i in range(1, Nt + 1):
            xt[i] = x[1]
            yt[i] = x[2]
            zt[i] = x[3]
            self.RK4(x, self.sys_state_space_dims, timev, self.h, x)
            timev += self.h

        # print("xt:", xt)
        # print("yt:", yt)
        # print("zt:", zt)

        # Downsample to ECG sampling frequency:
        xts = [0 for e in range(Nt + 1)]  # new double[Nt + 1];
        yts = [0 for e in range(Nt + 1)]  # new double[Nt + 1];
        zts = [0 for e in range(Nt + 1)]  # new double[Nt + 1];

        j = 0
        i = 1
        while i <= Nt:
            j += 1
            xts[j] = xt[i]
            yts[j] = yt[i]
            zts[j] = zt[i]
            i += q

        Nts = j

        # Do peak detection using angle
        ipeak = [0 for e in range(Nts + 1)]  # new double[Nts + 1];
        self.detect_peaks(ipeak, xts, yts, zts, Nts)

        # Scale signal to lie between -0.4 and 1.2 mV :
        zmin = zts[1]
        zmax = zts[1]

        for i in range(2, Nts + 1):
            if zts[i] < zmin:
                zmin = zts[i]
            elif zts[i] > zmax:
                zmax = zts[i]

        zrange = zmax - zmin
        # for (i=1; i <= Nts; i++)

        for i in range(1, Nts + 1):
            if zrange == 0.0:
                zts[i] = math.inf - 0.4
            else:
                zts[i] = (zts[i] - zmin) * (1.6) / zrange - 0.4

        # Include additive uniformly distributed measurement noise:
        # for (i=1; i <= Nts; i++)
        for i in range(1, Nts + 1):
            zts[i] += self.ecg_params.getANoise() * (2.0 * self.ran1() - 1.0)

        # Insert into the ECG data table:
        # print("Generating result matrix...")

        self.ecg_result_num_rows = Nts

        self.ecg_result_time = [0 for e in range(self.ecg_result_num_rows)]
        self.ecg_result_voltage = [0 for e in range(self.ecg_result_num_rows)]
        self.ecg_result_peaks = [0 for e in range(self.ecg_result_num_rows)]

        # for (i=1;i <= Nts;i++){
        for i in range(1, Nts + 1):
            self.ecg_result_time[i-1] = (i-1) * tstep
            self.ecg_result_voltage[i-1] = zts[i]
            self.ecg_result_peaks[i-1] = int(ipeak[i])

        # print("Finished generating result matrix.")
        return (RetValue)