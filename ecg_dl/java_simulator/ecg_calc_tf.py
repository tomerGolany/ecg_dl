import tensorflow as tf
import math
# Implementation of the ecg simulator with tensorflow graph:


class EcgCalc:
    def __init__(self, ecg_parameters):
        """

        """
        # Init constants : #####################################################

        self.PI = tf.constant(math.pi, name="pi")
        self.NR_END = tf.constant(1, name="nr_end")
        self.IA = tf.constant(16807, name="IA")
        self.IM = tf.constant(2147483647, name="IM")
        self.AM = (tf.constant(1.0) / tf.to_float(self.IM))
        self.IQ = tf.constant(127773, name="IQ")
        self.IR = tf.constant(2836, name="IR")
        self.NTAB = tf.constant(32, name="NTAB")
        self.NDIV = tf.constant(1.0) + tf.to_float(self.IM - tf.constant(1)) / tf.to_float(self.NTAB)
        self.EPS = tf.constant(1.2e-7, name="EPS")
        self.RNMX = (1.0 - self.EPS)

        #######################################################################

        # Init variables :
        # Order of extrema: [P Q R S T]:
        self.theta_i = tf.get_variable("theta_i", shape=[6], initializer=tf.zeros_initializer)  #  New calculated theta converted to radians
        self.a_i = tf.get_variable("a_i", shape=[6], initializer=tf.zeros_initializer)  # New calculated a
        self.b_i = tf.get_variable("b_i", shape=[6], initializer=tf.zeros_initializer)  # New calculated b

        self.num_of_ecg_outputs = tf.constant(0, name="num_of_ecg_outputs" )
        self.sys_state_space_dims = tf.constant(3, name="system_state_space_dims")
        self.x_initial_value = tf.constant(1.0, name="x_initial_value")
        self.y_initial_vale = tf.constant(0.0, name="y_initial_value")
        self.z_initial_value = tf.constant(0.04, name="z_initial_value")

        self.rseed = None  # Long type
        self.h = None
        self.rr = None
        self.rrpc = None

        # ECG result variables :
        # result vectors :
        self.ecg_result_time = None
        self.ecg_result_voltage = None
        self.ecg_result_peaks = None

        self.ecg_result_num_rows = None

        self.ecg_params = ecg_parameters

        # Variables for the static function rang()
        self.iy = tf.get_variable("iy", shape=[], initializer=tf.zeros_initializer)
        # self.iv = tf.get_variable("iv", shape=[32], initializer=tf.zeros_initializer)  # later we will fill this with elements of type Long ?.
        self.iv = tf.Variable([], name="iv")
    def calculate_ecg(self):
        """

        :return:
        """
        ret_value = True
        print("Starting to calculate ECG.....")

        ret_value = self.run_ecg_calcs()

        print("Finished calculating ECG table data")

        return ret_value

    def ran1_cond1(self):
        """

        :return:
        """
        self.rseed = tf.cond(tf.less(tf.constant(-1) * self.rseed, tf.constant(1)), lambda: 1, lambda: -1 * self.rseed)
        j = self.NTAB + tf.constant(7)
        cond = lambda j0: j0 >= 0
        body = lambda j0: self.run1_while1_body(j0)
        tf.while_loop(cond, body, [j])
        self.iy = self.iv[0]
        return True

    def run1_cond2(self, j):
        val = tf.to_float(self.rseed)
        self.iv = tf.concat([[val], self.iv], 0)
        return True

    def run1_while1_body(self, j):
        """

        :return:
        """

        k = tf.to_int32(self.rseed / self.IQ)
        self.rseed = self.IA * (self.rseed - k * self.IQ) - self.IR * k
        self.rseed = tf.cond(tf.less(self.rseed, tf.constant(0)), lambda: self.rseed + self.IM, lambda: self.rseed)
        # self.iv[j] = tf.cond(tf.less(j, self.NTAB), lambda: tf.to_float(self.rseed), lambda: self.iv[j])
        # res = tf.cond(tf.less(j, self.NTAB), lambda: tf.to_float(self.rseed), lambda: self.iv[j])
        tf.cond(tf.less(j, self.NTAB), lambda: self.run1_cond2(j), lambda: False)
        j = j - tf.constant(1)
        return j

    def ran1_body_x(self, i, j, iv_copy):
        val = self.iv[i]
        iv_copy = tf.concat([iv_copy, [val]], 0)
        return (i + 1, j, iv_copy)

    def ran1_body_x2(self, i, n, iv_copy):
        val = self.iv[i]
        iv_copy = tf.concat([iv_copy, [val]], 0)
        return (i + 1, n, iv_copy)

    def ran1(self):
        """
        TODO: Give a better name
        :return:
        """
        flg = tf.cond(tf.equal(self.iy, tf.constant(0.0)), lambda: tf.constant(False, dtype=bool), lambda: tf.constant(True, dtype=bool))

        tf.cond(tf.logical_or(tf.less_equal(self.rseed, tf.constant(0)), tf.logical_not(flg)), lambda: self.ran1_cond1(), lambda: False)

        k = tf.to_int32(self.rseed / self.IQ)
        self.rseed = self.IA * (self.rseed - k * self.IQ) - self.IR * k

        self.rseed = tf.cond(tf.less(self.rseed, tf.constant(0)), lambda: self.rseed + self.IM, lambda: self.rseed)

        j = tf.to_int32(self.iy / self.NDIV)
        self.iy = self.iv[j]
        # workaround:
        # length_of_iv = self.iv.get_shape().as_list()[0]
        length_of_iv = tf.constant(32)
        iv_copy = tf.Variable([], name="iv_copy")
        i = tf.constant(0)
        cond_x = lambda i0, j0, iv_copy_x: i0 < j0
        body_x = lambda i0, j0, iv_copy_x: self.ran1_body_x(i0, j0, iv_copy_x)
        tf.while_loop(cond_x, body_x, loop_vars=[i, j, iv_copy], shape_invariants=[i.get_shape(), j.get_shape(), tf.TensorShape([None])])
        # self.iv[j] = self.rseed
        iv_copy = tf.concat([iv_copy, [tf.to_float(self.rseed)]], 0)

        i2 = j + tf.constant(1)
        cond_x2 = lambda i0, n0, iv_copy_x: i0 < n0
        body_x2 = lambda i0, n0, iv_copy_x: self.ran1_body_x2(i0, n0, iv_copy_x)
        tf.while_loop(cond_x2, body_x2, loop_vars=[i2, length_of_iv, iv_copy],
                      shape_invariants=[i2.get_shape(), length_of_iv.get_shape(), tf.TensorShape([None])])

        self.iv = iv_copy

        ret_value = tf.cond(tf.greater((self.AM * self.iy), self.RNMX), lambda: self.RNMX, lambda: self.AM * self.iy)

        return ret_value

    def swap(self, x, y):
        """

        :param x:
        :param y:
        :return:
        """
        swap = x
        x = y
        y = swap

    def ifft_while1_body(self, i, n, data, j):
        """

        :param i:
        :param n:
        :return:
        """
        tf.cond(tf.greater(j, i), self.swap(data[tf.to_int64(i)], data[tf.to_int64(j)]), None)
        tf.cond(tf.greater(j, i), self.swap(data[tf.to_int64(i) + 1], data[tf.to_int64(j) + 1]), None)

        m = n / tf.constant(2.0)
        cond2 = lambda m0, j0: m0 >= tf.constant(2.0) and j0 > m0
        body2 = lambda m0, j0: (m0 / tf.constant(2.0), j0 - m0)
        tf.while_loop(cond2, body2, (m, j))

        j = j + m
        i = i + tf.constant(2)
        return (i, n, data, j)


    def ifft_while4_body(self, i, n, data, istep, wi, wr, mmax):
        """

        :param i:
        :param n:
        :param data:
        :param istep:
        :param wi:
        :param wr:
        :return:
        """
        j = i + mmax
        tempr = wr * data[tf.to_int64(j)] - wi * data[tf.to_int64(j) + tf.constant(1)]
        tempi = wr * data[tf.to_int64(j) + tf.constant(1)] + wi * data[tf.to_int64(j)]

        data[tf.to_int64(j)] = data[tf.to_int64(i)] - tempr
        data[tf.to_int64(j) + tf.constant(1)] = data[tf.to_int64(i) + tf.constant(1)] - tempi
        data[tf.to_int64(i)] = data[tf.to_int64(i)] + tempr
        data[tf.to_int64(i) + tf.constant(1)] = data[tf.to_int64(i) + tf.constant(1)] + tempi

        i = i + istep
        return (i, n, data, istep, wi, wr, mmax)


    def ifft_while3_body(self, m, mmax, data, istep, wtemp, wr, wi, wpi, wpr, n):
        """

        :param m:
        :param mmax:
        :param data:
        :param istep:
        :param wtemp:
        :param wr:
        :param wi:
        :param wpi:
        :param wpr:
        :return:
        """
        i = m
        cond = lambda i0, n0, data0, istep0, wi0, wr0, mmax0: i0 <= n0
        body = lambda i0, n0, data0, istep0, wi0, wr0, mmax0: self.ifft_while4_body(i0, n0, data0, istep0, wi0, wr0, mmax0)
        tf.while_loop(cond, body, (i, n, data, istep, wi, wr, mmax))

        wtemp = wr
        wr = wtemp * wpr - wi * wpi + wr
        wi = wi * wpr + wtemp * wpi + wi
        m = m + tf.constant(2)
        return (m, mmax, data, istep, wtemp, wr, wi, wpi, wpr, n)

    def ifft_while2_body(self, n, mmax, isign, data):
        """

        :param n:
        :param mmax:
        :param isign:
        :param data:
        :return:
        """
        istep = mmax * tf.constant(2)
        theta = isign * (tf.constant(6.28318530717959) / tf.to_float(mmax))
        wtemp = tf.sin(tf.constant(0.5) * theta)
        wpr = tf.constant(-2.0) * wtemp * wtemp
        wpi = tf.sin(theta)
        wr = tf.constant(1.0)
        wi = tf.constant(0.0)

        m = tf.constant(1)
        cond = lambda m0, mmax0, data0, istep0, wtemp0, wr0, wi0, wpi0, wpr0, n0: m0 < mmax0
        body = lambda m0, mmax0, data0, istep0, wtemp0, wr0, wi0, wpi0, wpr0, n0: self.ifft_while3_body(m0, mmax0, data0, istep0, wtemp0, wr0, wi0, wpi0, wpr0, n0)
        tf.while_loop(cond, body, loop_vars=(m, mmax, data, istep, wtemp, wr, wi, wpi, wpr, n))

        mmax = istep
        return (n, mmax, isign, data)

    def ifft(self, data, nn, isign):
        """

        :param data:
        :param nn:
        :param isign:
        :return:
        """
        with tf.variable_scope("ifft"):
            n = nn * tf.constant(2)
            i = tf.constant(1)
            j = tf.constant(1)
            # First while Loop :
            cond1 = lambda i0, n0, data0, j0: tf.less(i0, n0)
            body1 = lambda i0, n0, data0, j0: self.ifft_while1_body(i0, n0, data0, j0)
            tf.while_loop(cond1, body1, loop_vars=(i, n, data, j))

            mmax = tf.constant(2)

            cond2 = lambda n0, mmax0, isign0, data0: n0 > mmax0
            body2 = lambda n0, mmax0, isign0, data0: self.ifft_while2_body(n0, mmax0, isign0, data0)
            tf.while_loop(cond2, body2, loop_vars=(n, mmax, isign, data))

    def stdev_calc(self, x, n):
        """

        :param x:
        :param n:
        :return:
        """
        with tf.variable_scope("stdev_calc"):
            add = tf.constant(0.0)
            j = tf.constant(1)
            cond = lambda j0, n0, x0, add0: j0 <= n0
            body = lambda j0, n0, x0, add0: (j0 + 1, n0, x0, add0 + x0[j0])
            tf.while_loop(cond, body, loop_vars=(j, n, x, add))

            mean = tf.to_float(add) / tf.to_float(n)

            total = tf.constant(0.0)
            j = tf.constant(1)
            cond = lambda j0, n0, x0, total0, mean0: j0 < n + tf.constant(1)
            body = lambda j0, n0, x0, total0, mean0: (j0 + tf.constant(1), n0, x0, total0 + (x0[j0] - mean0) * (x0[j0] - mean0), mean0)
            tf.while_loop(cond, body, loop_vars=(j, n, x, total, mean))

            return tf.sqrt(tf.to_float(total) / tf.to_float(n - tf.constant()))

    def ang_freq_calc(self, t):
        """
        THE ANGULAR FREQUENCY
        :param t:
        :return:
        """

        i = tf.constant(1) + tf.to_int64(tf.floor(t / self.h))
        ret_value = tf.cond(tf.equal(self.rrpc[i], tf.constant(0.0)), lambda: tf.constant(math.inf), lambda: tf.constant(2.0) * self.PI / tf.to_float(self.rrpc[i]))
        return ret_value

    def derive_pqrst_while_body(self, i, k, xi, yi):
        """

        :param i:
        :param k:
        :param xi:
        :param yi:
        :return:
        """
        xi[i] = tf.cos(self.theta_i[i])
        yi[i] = tf.sin(self.theta_i[i])

        return (i + 1, k, xi, yi)

    def derive_pqrst_while2_body(self, i, k, t, dxdt):
        """

        :param i:
        :param k:
        :param t:
        :param dxdt:
        :return:
        """
        # dt = math.fmod(t - self.theta_i[i], 2.0 * self.PI)
        a = t - self.theta_i[i]
        b = tf.constant(2.0) * self.PI
        c = a/b
        dt = a - tf.to_int32(c) * b
        dt2 = dt * dt
        dxdt[3] = dxdt + tf.constant(-1) * self.a_i[i] * dt * tf.exp(tf.constant(-0.5) * dt2 / (self.b_i[i] * self.b_i[i]))

        return (i + 1, k, t, dxdt)

    def derivs_pqrst_calc(self, t0, x, dxdt):
        """
        THE EXACT NONLINEAR DERIVATIVES
        :param t0:
        :param x:
        :param dxdt:
        :return:
        """

        k = tf.constant(5)
        xi = tf.get_variable("xi", shape=[k+tf.constant(1)], initializer=tf.zeros_initializer)  # [0 for e in range(k+1)]  # double[k + 1];
        yi = tf.get_variable("yi", shape=[k+tf.constant(1)], initializer=tf.zeros_initializer)
        w0 = self.ang_freq_calc(t0)
        r0 = tf.constant(1.0)
        x0 = tf.constant(0.0)
        y0 = tf.constant(0.0)
        z0 = tf.constant(0.0)
        a0 = tf.constant(1.0) - tf.sqrt((x[1] - x0) * (x[1] - x0) + (x[2] - y0) * (x[2] - y0)) / r0

        i = tf.constant(1)
        cond = lambda i0, k0, xi0, yi0: i0 < k0 + tf.constant(1)
        body = lambda i0, k0, xi0, yi0: self.derive_pqrst_while_body(i0, k0, xi0, yi0)
        tf.while_loop(cond, body, loop_vars=(i, k, xi, yi))

        zbase = tf.constant(0.005) * tf.sin(tf.constant(2.0) * self.PI * self.ecg_params.getFHi() * t0)

        t = tf.atan2(x[2], x[1])
        dxdt[1] = a0 * (x[1] - x0) - w0 * (x[2] - y0)
        dxdt[2] = a0 * (x[2] - y0) + w0 * (x[1] - x0)

        dxdt[3] = tf.constant(0.0)

        i = tf.constant(1)
        cond2 = lambda i0, k0, t0, dxdt0: i0 < k0 + tf.constant(1)
        body2 = lambda i0, k0, t0, dxdt0: self.derive_pqrst_while2_body(i0, k0, t0, dxdt0)
        tf.while_loop(cond2, body2, loop_vars=(i, k, t, dxdt))

        dxdt[3] = dxdt[3] + tf.constant(-1.0) * (x[3] - zbase)


    def rk4_while_body1(self, i, n, yt, y, hh, dydx):

        yt[i] = y[i] + hh * dydx[i]
        return (i + 1, n, yt, y, hh, dydx)

    def rk4_while_body2(self, i, n, yt, y, hh, dyt):

        yt[i] = y[i] + hh * dyt[i]
        return (i + 1, n, yt, y, hh, dyt)

    def rk4_while_body3(self, i, n, yt, y, dym, dyt, h):
        yt[i] = y[i] + h * dym[i]
        dym[i] += dyt[i]

        return (i + 1, n, yt, y, dym, dyt, h)

    def rk4_while_body4(self, i, n, yout, y, h6, dym, dydx, dyt):
        yout[i] = y[i] + h6 * (dydx[i] + dyt[i] + tf.constant(2.0) * dym[i])

        return (i + 1, n, yout, y, h6, dym, dydx, dyt)

    def RK4(self, y, n, x, h, yout):
        """
        RUNGA-KUTTA FOURTH ORDER INTEGRATION
        :param y:
        :param n:
        :param x:
        :param h:
        :param yout:
        :return:
        """
        with tf.name_scope("RK4"):
            dydx = tf.get_variable("dydx", shape=[None], initializer=tf.zeros_initializer)  # double[n + 1];
            dym = tf.get_variable("dydx", shape=[None], initializer=tf.zeros_initializer)  # double[n + 1];
            dyt = tf.get_variable("dydx", shape=[None], initializer=tf.zeros_initializer)  # double[n + 1];
            yt = tf.get_variable("dydx", shape=[None], initializer=tf.zeros_initializer)  # double[n + 1];

            hh = h * tf.constant(0.5)
            h6 = h / tf.constant(6.0)
            xh = x + hh

            self.derivs_pqrst_calc(x, y, dydx)

            i = tf.constant(1)
            cond1 = lambda i0, n0, yt0, y0, hh0, dydx0: i0 < n0 + tf.constant(1)
            body1 = lambda i0, n0, yt0, y0, hh0, dydx0: self.rk4_while_body1(i0, n0, yt0, y0, hh0, dydx0)
            tf.while_loop(cond1, body1, loop_vars=(i, n, yt, y, hh, dydx))

            self.derivs_pqrst_calc(xh, yt, dyt)

            i2 = tf.constant(1)
            cond2 = lambda i0, n0, yt0, y0, hh0, dyt0: i0 < n0 + tf.constant(1)
            body2 = lambda i0, n0, yt0, y0, hh0, dyt0: self.rk4_while_body2(i0, n0, yt0, y0, hh0, dyt0)
            tf.while_loop(cond2, body2, loop_vars=(i2, n, yt, y, hh, dyt))

            self.derivs_pqrst_calc(xh, yt, dym)

            i3 = tf.constant(1)
            cond3 = lambda i0, n0, yt0, y0, dym0, dyt0, h0: i0 < n0 + tf.constant(1)
            body3 = lambda i0, n0, yt0, y0, dym0, dyt0, h0: self.rk4_while_body3(i0, n0, yt0, y0, dym0, dyt0, h0)
            tf.while_loop(cond3, body3, loop_vars=(i3, n, yt, y, dym, dyt, h))

            self.derivs_pqrst_calc(x + h, yt, dyt)

            i4 = tf.constant(1)
            cond4 = lambda i0, n0, yout0, y0, h60, dym0, dydx0, dyt0: i0 < n0 + tf.constant(1)
            body4 = lambda i0, n0, yout0, y0, h60, dym0, dydx0, dyt0: self.rk4_while_body4(i0, n0, yout0, y0, h60, dym0, dydx0, dyt0)
            tf.while_loop(cond4, body4, loop_vars=(i4, n, yout, y, h6, dym, dydx, dyt))

    def rrprocess_while_body1(self, i, n, w, df):
        val = (tf.to_float(i - tf.constant(1))) * tf.constant(2.0) * self.PI * df
        w = tf.concat([w, [val]], 0)
        # w[i] = (tf.to_float(i - tf.constant(1))) * tf.constant(2.0) * self.PI * df
        return (i + 1, n, w, df)

    def rrprocess_while_body2(self, i, n, Hw, sig1, w1, c1, sig2, w2, c2, w):
        val = (sig1 * tf.exp(tf.constant(-0.5) * (tf.pow(w[i] - w1, tf.constant(2.0)) / tf.pow(c1, tf.constant(2.0)))) /
                 tf.sqrt(tf.constant(2.0) * self.PI * c1 * c1)) + \
                (sig2 * tf.exp(tf.constant(-0.5) * (tf.pow(w[i] - w2, tf.constant(2.0)) / tf.pow(c2, tf.constant(2.0)))) / tf.sqrt(tf.constant(2.0) * self.PI * c2 * c2))
        Hw = tf.concat([Hw, [val]], 0)

        return (i+1, n, Hw, sig1, w1, c1, sig2, w2, c2, w)


    def rrprocess_while_body3(self, i, n, Sw, sf, Hw):
        val = (sf / tf.constant(2.0)) * tf.sqrt(Hw[i])
        Sw = tf.concat([Sw, [val]], 0)
        return (i + 1, n, Sw, sf, Hw)

    def rrprocess_while_body4(self, i, n, Sw, sf, Hw):
        val = (sf / tf.constant(2.0)) * tf.sqrt(Hw[n - i + tf.constant(1)])
        Sw = tf.concat([Sw, [val]], 0)
        return (i + 1, n, Sw, sf, Hw)

    def rrprocess_while_body5(self, i, n, ph0):
        val = 2.0 * self.PI * self.ran1()
        ph0 = tf.concat([ph0, [val]], 0)
        return (i + 1, n, ph0)

    def rrprocess_while_body6(self, i, n, ph, ph0):
        val = ph0[i]
        # ph[i + 1] = ph0[i]
        ph = tf.concat([ph, [val]], 0)
        return (i + 1, n, ph, ph0)

    def rrprocess_while_body7(self, i, n, ph, ph0):
        val = tf.constant(-1.0) * ph0[i]
        # ph[n - i + tf.constant(1)] = tf.constant(-1.0) * ph0[i]
        ph = tf.concat([[val], ph], 0)
        return (i + 1, n, ph, ph0)

    def rrprocess_while_body8(self, i, n, SwC, Sw, ph):
        SwC[2 * i - 1] = Sw[i] * tf.cos(ph[i])
        return (i + 1, n, SwC, Sw, ph)

    def rrprocess_while_body9(self, i, n, SwC, Sw, ph):
        SwC[2 * i] = Sw[i] * tf.sin(ph[i])
        return (i + 1, n, SwC, Sw, ph)

    def rrprocess_while_body10(self, i, n, SwC, rr):
        rr[i] = (tf.constant(1.0) / tf.to_float(n)) * SwC[2 * i - 1]
        return (i + 1, n, SwC, rr)

    def rrprocess_while_body11(self, i, n, rr, ratio):
        rr[i] = rr[i] * ratio
        return (i + 1, n, rr, ratio)

    def rrprocess_while_body12(self, i, n, rr, rrmean):
        rr[i] = rr[i] + rrmean
        return (i + 1, n, rr, rrmean)

    def rrprocess_while_body_special1(self, i, j, ph_copy, ph):
        val = ph[i]
        ph_copy = tf.concat([ph_copy, [val]], 0)
        return (i + 1, j, ph_copy, ph)

    def rrprocess_while_body_special2(self, i, j, ph_copy, ph):
        val = ph[i]
        ph_copy = tf.concat([ph_copy, [val]], 0)
        return (i + 1, j, ph_copy, ph)

    def rrprocess_while_body_special3(self, i, n, ph, ph_copy):
        val = ph[i]
        ph_copy = tf.concat([[val], ph_copy], 0)
        return (i + 1, n, ph, ph_copy)

    def rrprocess(self, rr, flo, fhi, flostd, fhistd, lfhfratio, hrmean, hrstd, sf, n):
        """
        GENERATE RR PROCESS
        :param rr:
        :param flo:
        :param fhi:
        :param flostd:
        :param fhistd:
        :param lfhfratio:
        :param hrmean:
        :param hrstd:
        :param sf:
        :param n:
        :return:
        """
        with tf.variable_scope("rrprocess"):

            w = tf.Variable([], name="w")  # new double[n + 1];
            Hw = tf.Variable([], name="Hw",)  # new double[n + 1];
            Sw = tf.Variable([], name="Sw")  # new double[n + 1];

            # ph0_len = tf.to_int32(n / tf.constant(2.0) - tf.constant(1) + tf.constant(1))
            ph0 = tf.Variable([], name="ph0")  # new double[(int)(n / 2 - 1 + 1)];
            ph = tf.Variable([], name="ph")  # [0 for e in range(n+1)]
            SwC = tf.Variable([], name="SwC")  # [0 for e in range(2 *n + 1)]  # new double[(2 * n) + 1];

            w = tf.concat([w, [0.0]], 0)
            Hw = tf.concat([Hw, [0.0]], 0)
            Sw = tf.concat([Sw, [0.0]], 0)
            ph0 = tf.concat([ph0, [0.0]], 0)
            ph = tf.concat([ph, [0.0]], 0)
            SwC = tf.concat([SwC, [0.0]], 0)

            w1 = tf.constant(2.0) * self.PI * flo
            w2 = tf.constant(2.0) * self.PI * fhi
            c1 = tf.constant(2.0) * self.PI * flostd
            c2 = tf.constant(2.0) * self.PI * fhistd
            sig2 = tf.constant(1.0)
            sig1 = lfhfratio
            rrmean = tf.constant(60.0) / hrmean
            rrstd = tf.constant(60.0) * hrstd / (hrmean * hrmean)

            df = sf / tf.to_float(n)

            i1 = tf.constant(1)
            cond1 = lambda i0, n0, w0, df0: i0 < n0 + tf.constant(1)
            body1 = lambda i0, n0, w0, df0: self.rrprocess_while_body1(i0, n0, w0, df0)
            tf.while_loop(cond1, body1, loop_vars=[i1, n, w, df], shape_invariants=[i1.get_shape(), n.get_shape(),
                                                   tf.TensorShape([None]), df.get_shape()])

            i2 = tf.constant(1)
            cond2 = lambda i0, n0, Hw0, sig10, w10, c10, sig20, w20, c20, w0: i0 < n0 + tf.constant(1)
            body2 = lambda i0, n0, Hw0, sig10, w10, c10, sig20, w20, c20, w0: self.rrprocess_while_body2(i0, n0, Hw0, sig10, w10, c10, sig20, w20, c20, w0)
            tf.while_loop(cond2, body2, loop_vars=[i2, n, Hw, sig1, w1, c1, sig2, w2, c2, w], shape_invariants=[i2.get_shape(), n.get_shape(),
                                                   tf.TensorShape([None]), sig1.get_shape(), w1.get_shape() ,c1.get_shape() ,
                                                sig2.get_shape() , w2.get_shape(), c2.get_shape(), w.get_shape()])

            i3 = tf.constant(1)
            cond3 = lambda i0, n0, Sw0, sf0, Hw0: i0 < tf.to_int32(n0/2) + tf.constant(1)
            body3 = lambda i0, n0, Sw0, sf0, Hw0: self.rrprocess_while_body3(i0, n0, Sw0, sf0, Hw0)
            tf.while_loop(cond3, body3, loop_vars=[i3, n, Sw, sf, Hw], shape_invariants=[i3.get_shape(), n.get_shape(),
                                                   tf.TensorShape([None]), sf.get_shape(), Hw.get_shape()])

            i4 = tf.to_int32(n/2) + tf.constant(1)
            cond4 = lambda i0, n0, Sw0, sf0, Hw0: i0 < n0 + tf.constant(1)
            body4 = lambda i0, n0, Sw0, sf0, Hw0: self.rrprocess_while_body4(i0, n0, Sw0, sf0, Hw0)
            tf.while_loop(cond4, body4, loop_vars=[i4, n, Sw, sf, Hw], shape_invariants=[i4.get_shape(), n.get_shape(),
                                                   tf.TensorShape([None]), sf.get_shape(), Hw.get_shape()])

            i5 = tf.constant(1)
            cond5 = lambda i0, n0, ph00: i0 < tf.to_int32(n0/2)
            body5 = lambda i0, n0, ph00: self.rrprocess_while_body5(i0, n0, ph00)
            tf.while_loop(cond5, body5, loop_vars=[i5, n, ph0], shape_invariants=[i5.get_shape(), n.get_shape(),
                                                   tf.TensorShape([None])])

            ph = tf.concat([ph, [tf.constant(0.0)]], 0)

            i6 = tf.constant(1)
            cond6 = lambda i0, n0, ph0, ph00: i0 < tf.to_int32(n0 / 2)
            body6 = lambda i0, n0, ph0,ph00: self.rrprocess_while_body6(i0, n0, ph0, ph00)
            tf.while_loop(cond6, body6, loop_vars=[i6, n, ph, ph0], shape_invariants=[i6.get_shape(), n.get_shape(),
                                                   tf.TensorShape([None]), ph0.get_shape()])



            j_special1 = tf.to_int32(n / tf.constant(2)) + tf.constant(1)
            i_special1 = tf.constant(0)
            ph_copy = tf.Variable([], name="ph_copy")
            cond_special1 = lambda i0, j0, ph_copy0, ph00: i0 < j0
            body_special1 = lambda i0, j0, ph_copy0, ph00: self.rrprocess_while_body_special1(i0, j0, ph_copy0, ph00)
            tf.while_loop(cond_special1, body_special1, loop_vars=[i_special1, j_special1, ph_copy, ph], shape_invariants=[i_special1.get_shape(), j_special1.get_shape(),
                                                                                      tf.TensorShape([None]), ph.get_shape()])

            # ph[tf.to_int32(n / tf.constant(2)) + tf.constant(1)] = tf.constant(0.0)

            ph_copy = tf.concat([ph_copy, [tf.constant(0.0)]], 0)

            j_special2 = n + tf.constant(1)
            i_special2 = j_special1 + tf.constant(1)
            cond_special2 = lambda i0, j0, ph_copy0, ph0: i0 < j0
            body_special2 = lambda i0, j0, ph_copy0, ph0: self.rrprocess_while_body_special2(i0, j0, ph_copy0, ph0)
            tf.while_loop(cond_special2, body_special2, loop_vars=[i_special2, j_special2, ph_copy, ph],
                          shape_invariants=[i_special1.get_shape(), j_special1.get_shape(),
                                            tf.TensorShape([None]), ph.get_shape()])

            ph = ph_copy

            # More workarounds:
            ph_copy2 = tf.Variable([], name="ph_copy2")
            i7 = tf.constant(1)
            cond7 = lambda i0, n0, ph0, ph00: i0 < tf.to_int32(n0 / 2)
            body7 = lambda i0, n0, ph0, ph00: self.rrprocess_while_body7(i0, n0, ph0, ph00)
            tf.while_loop(cond7, body7, loop_vars=[i7, n, ph_copy2, ph0], shape_invariants=[i7.get_shape(), n.get_shape(),
                                            tf.TensorShape([None]), ph0.get_shape()])

            i_special3 = tf.to_int32(n / 2)
            cond_special3 = lambda i0, n0, ph00, ph_copy2_0: i0 < n0
            body_special3 = lambda i0, n0, ph00, ph_copy2_0: self.rrprocess_while_body_special3(i0, n0, ph00, ph_copy2_0)
            tf.while_loop(cond_special3, body_special3, loop_vars=[i_special3, n, ph, ph_copy2], shape_invariants=[i_special3.get_shape(), n.get_shape(),
                                                                                                                   ph.get_shape(), tf.TensorShape([None])])

            # make complex spectrum :
            i8 = tf.constant(1)
            cond8 = lambda i0, n0, SwC0, Sw0, ph0: i0 < n0 + tf.constant(1)
            body8 = lambda i0, n0, SwC0, Sw0, ph0: self.rrprocess_while_body8(i0, n0, SwC0, Sw0, ph0)
            tf.while_loop(cond8, body8, loop_vars=[i8, n, SwC, Sw, ph])

            i9 = tf.constant(1)
            cond9 = lambda i0, n0, SwC0, Sw0, ph0: i0 < n0 + tf.constant(1)
            body9 = lambda i0, n0, SwC0, Sw0, ph0: self.rrprocess_while_body9(i0, n0, SwC0, Sw0, ph0)
            tf.while_loop(cond9, body9, loop_vars=[i9, n, SwC, Sw, ph])

            # Calclate inverse fft:
            self.ifft(SwC, n, tf.constant(-1))


            # Excecute real part :
            i10 = tf.constant(1)
            cond10 = lambda i0, n0, SwC0, rr0: i0 < n0 + tf.constant(1)
            body10 = lambda i0, n0, SwC0, rr0: self.rrprocess_while_body10(i0, n0, SwC0, rr0)
            tf.while_loop(cond10, body10, loop_vars=(i10, n, SwC, rr))

            xstd = self.stdev_calc(rr, n)
            ratio = rrstd / xstd

            i11 = tf.constant(1)
            cond11 = lambda i0, n0, rr0, ratio0: i0 < n0 + tf.constant(1)
            body11 = lambda i0, n0, rr0, ratio0: self.rrprocess_while_body11(i0, n0, rr0, ratio0)
            tf.while_loop(cond11, body11, loop_vars=(i11, n, rr, ratio))

            i12 = tf.constant(1)
            cond12 = lambda i0, n0, rr0, rrmean0: i0 < n0 + tf.constant(1)
            body12 = lambda i0, n0, rr0, rrmean0: self.rrprocess_while_body12(i0, n0, rr0, rrmean0)
            tf.while_loop(cond12, body12, loop_vars=(i12, n, rr, rrmean))


    def detect_peaks_while_body1(self, i, n, ipeak):
        ipeak[i] = 0.0
        return (i + 1, n, ipeak)

    def detect_peaks_while_body2(self, i, n, ipeak, thetap1, thetap2, thetap3, thetap4, thetap5, y, x, theta1):
        theta2 = tf.atan2(y[i + 1], x[i + 1])

        ipeak[i] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap1), tf.less_equal(thetap1, theta2)), tf.less(thetap1 - theta1, theta2 - thetap1)), lambda: tf.constant(1.0), None)
        ipeak[i + 1] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap1), tf.less_equal(thetap1, theta2)), tf.greater_equal(thetap1 - theta1, theta2 - thetap1)), lambda: tf.constant(1.0), None)

        ipeak[i] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap2), tf.less_equal(thetap2, theta2)), tf.less(thetap2 - theta1, theta2 - thetap2)), lambda: tf.constant(2.0), None)
        ipeak[i + 1] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap2), tf.less_equal(thetap2, theta2)), tf.greater_equal(thetap2 - theta1, theta2 - thetap2)), lambda: tf.constant(2.0), None)

        ipeak[i] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap3), tf.less_equal(thetap3, theta2)), tf.less(thetap3 - theta1, theta2 - thetap3)), lambda: tf.constant(3.0), None)
        ipeak[i + 1] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap3), tf.less_equal(thetap3, theta2)), tf.greater_equal(thetap3 - theta1, theta2 - thetap3)), lambda: tf.constant(3.0), None)

        ipeak[i] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap4), tf.less_equal(thetap4, theta2)), tf.less(thetap4 - theta1, theta2 - thetap4)), lambda: tf.constant(4.0), None)
        ipeak[i + 1] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap4), tf.less_equal(thetap4, theta2)), tf.greater_equal(thetap4 - theta1, theta2 - thetap4)), lambda: tf.constant(4.0), None)

        ipeak[i] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap5), tf.less_equal(thetap5, theta2)), tf.less(thetap5 - theta1, theta2 - thetap5)), lambda: tf.constant(5.0), None)
        ipeak[i + 1] = tf.cond(tf.logical_and(tf.logical_and(tf.less_equal(theta1, thetap5), tf.less_equal(thetap5, theta2)), tf.greater_equal(thetap5 - theta1, theta2 - thetap5)), lambda: tf.constant(5.0), None)

        theta1 = theta2

        return (i + 1, n, ipeak, thetap1, thetap2, thetap3, thetap4, thetap5, y, x, theta1)

    def detect_peaks_while_body5(self, j, j1, j2, z, zmax, jmax):
        jmax = tf.cond(tf.greater(z[j], zmax), lambda: j, lambda: jmax)
        zmax = tf.cond(tf.greater(z[j], zmax), lambda: z[j], lambda: zmax)
        return (j + 1, j1, j2, z, zmax, jmax)

    def do_for_loop_detect_peaks_while_body3(self, j1, j2, z, zmax, jmax):
        j = j1 + tf.constant(1)
        cond4 = lambda j0, j10, j20, z0, zmax0, jmax0: j0 < j2 + tf.constant(1)
        body4 = lambda j0, j10, j20, z0, zmax0, jmax0: self.detect_peaks_while_body5(j0, j10, j20, z0, zmax0, jmax0)
        tf.while_loop(cond4, body4, loop_vars=(j, j1, j2, z, zmax, jmax))

    def detect_peaks_while_body7(self, j, j1, j2, z, zmin, jmin):
        jmin = tf.cond(tf.less(z[j], zmin), lambda: j, lambda: jmin)
        zmin = tf.cond(tf.less(z[j], zmin), lambda: z[j], lambda: zmin)
        return (j + 1, j1, j2, z, zmin, jmin)

    def do_for_loop_detect_peaks_while_body6(self, j1, j2, z, zmin, jmin):

        j = j1 + tf.constant(1)
        cond6 = lambda j0, j10, j20, z0, zmin0, jmin0: j0 < j2 + tf.constant(1)
        body6 = lambda j0, j10, j20, z0, zmin0, jmin0: self.detect_peaks_while_body7(j0, j10, j20, z0, zmin0, jmin0)

        tf.while_loop(cond6, body6, loop_vars=(j, j1, j2, z, zmin, jmin))


    def detect_peaks_while_body3(self, i, n, ipeak, d, z):

        res1 = tf.cond(tf.logical_or(tf.logical_or(tf.equal(ipeak[i], tf.constant(1.0)), tf.equal(ipeak[i], tf.constant(3.0))), tf.equal(ipeak[i], tf.constant(5.0))), lambda: True, lambda : False)

        j1 = tf.cond(tf.logical_and(tf.equal(res1, tf.constant(True, dtype=bool)), tf.greater(tf.constant(1.0), (i - d))), lambda: tf.constant(1), None)
        j1 = tf.cond(tf.logical_and(tf.equal(res1, tf.constant(True, dtype=bool)), tf.less_equal(tf.constant(1.0), (i - d))), lambda: i - d, lambda: j1)

        j2 = tf.cond(tf.logical_and(tf.equal(res1, tf.constant(True, dtype=bool)), tf.less(n, (i + d))), lambda: n, None)
        j2 = tf.cond(tf.logical_and(tf.equal(res1, tf.constant(True, dtype=bool)), tf.greater_equal(n, (i + d))), lambda: i + d, lambda: j2)

        jmax = tf.cond(tf.equal(res1, tf.constant(True, dtype=bool)), lambda: j1, None)
        zmax = tf.cond(tf.equal(res1, tf.constant(True, dtype=bool)), lambda: z[j1], None)

        tf.cond(tf.equal(res1, tf.constant(True, dtype=bool)), lambda: self.do_for_loop_detect_peaks_while_body3(j1, j2, z, zmax, jmax), None)

        ipeak[jmax] = tf.cond(tf.logical_and(tf.equal(res1, tf.constant(True, dtype=bool)), tf.not_equal(jmax, i)), lambda: ipeak[i], lambda: ipeak[jmax])
        ipeak[i] = tf.cond(tf.logical_and(tf.equal(res1, tf.constant(True, dtype=bool)), tf.not_equal(jmax, i)), lambda: tf.constant(0.0), lambda: ipeak[i])

        res2 = tf.cond(tf.logical_or(tf.equal(ipeak[i], tf.constant(2.0)), tf.equal(ipeak[i], tf.constant(4.0))), lambda: True, lambda: False)
        j1 = tf.cond(tf.logical_and(tf.equal(res2, tf.constant(True, dtype=bool)), tf.greater(tf.constant(1.0), i - d)), lambda: tf.constant(1.0), None)
        j1 = tf.cond(tf.logical_and(tf.equal(res2, tf.constant(True, dtype=bool)), tf.less_equal(tf.constant(1.0), i - d)), lambda: tf.constant(i-d), lambda: j1)

        j2 = tf.cond(tf.logical_and(tf.equal(res2, tf.constant(True, dtype=bool)), tf.less(n, i + d)), lambda: n, lambda: None)
        j2 = tf.cond(tf.logical_and(tf.equal(res2, tf.constant(True, dtype=bool)), tf.greater_equal(n, i + d)), lambda: i + d, lambda: j2)

        jmin = tf.cond(tf.equal(res2, tf.constant(True, dtype=bool)), lambda: j1, lambda: None)
        zmin = tf.cond(tf.equal(res2, tf.constant(True, dtype=bool)), lambda: z[j1], lambda: None)

        tf.cond(tf.equal(res2, tf.constant(True, dtype=bool)), lambda: self.do_for_loop_detect_peaks_while_body6(j1, j2, z, zmin, jmin), None)

        ipeak[jmin] = tf.cond(tf.logical_and(tf.equal(res2, tf.constant(True, dtype=bool)), tf.not_equal(jmin, i)), lambda: ipeak[i], lambda: ipeak[jmin])
        ipeak[i] = tf.cond(tf.logical_and(tf.equal(res2, tf.constant(True, dtype=bool)), tf.not_equal(jmin, i)), lambda: tf.constant(0.0), lambda: ipeak[i])

    def detect_peaks(self, ipeak, x, y, z, n):
        """

        :param ipeak:
        :param x:
        :param y:
        :param z:
        :param n:
        :return:
        """
        thetap1 = self.theta_i[1]
        thetap2 = self.theta_i[2]
        thetap3 = self.theta_i[3]
        thetap4 = self.theta_i[4]
        thetap5 = self.theta_i[5]

        i1 = 1
        cond1 = lambda i0, n0, ipeak0: i0 < n0 + tf.constant(1)
        body1 = lambda i0, n0, ipeak0: self.detect_peaks_while_body1(i0, n0, ipeak0)
        tf.while_loop(cond1, body1, loop_vars=(i1, n, ipeak))

        theta1 = tf.atan2(y[1], x[1])

        i2 = 1
        cond2 = lambda i0, n0, ipeak0, thetap10, thetap20, thetap30, thetap40, thetap50, y0, x0, theta10 : i0 < n0
        body2 = lambda i0, n0, ipeak0, thetap10, thetap20, thetap30, thetap40, thetap50, y0, x0, theta10: self.detect_peaks_while_body2(i0, n0, ipeak0, thetap10, thetap20, thetap30, thetap40, thetap50, y0, x0, theta10)
        tf.while_loop(cond2, body2, loop_vars=(i2, n, ipeak, thetap1, thetap2, thetap3, thetap4, thetap5, y, x, theta1))

        # Correct the peaks:
        d = tf.to_int32(tf.ceil(self.ecg_params.getSfEcg() / tf.constant(64)))

        i3 = 1
        cond3 = lambda i0, n0, ipeak0, d0, z0: i0 < n0 + tf.constant(1.0)
        body3 = lambda i0, n0, ipeak0, d0, z0: self.detect_peaks_while_body3(i0, n0, ipeak0, d0, z0)
        tf.while_loop(cond3, body3, loop_vars=(i3, n, ipeak, d, z))

    def ecg_calcs_while_body1(self, i):
        self.theta_i = self.theta_i[i].assign(self.ecg_params.getTheta(i - tf.constant(1)) * self.PI / tf.constant(180.0))
        self.a_i = self.a_i[i].assign(self.ecg_params.getA(i - tf.constant(1)))

        return (i + 1)

    def ecg_calcs_while_body2(self, i, hrfact):
        self.b_i = self.b_i[i].assign(self.ecg_params.getB(i - 1) * hrfact)
        return (i + 1, hrfact)

    def ecg_calcs_while_body4(self, k, i, j):
        self.rrpc = self.rrpc[k].assign(self.rr[i])
        return( k + 1, i, j)

    def ecg_calcs_while_body3(self, i, j, Nrr, tecg):
        tecg = tecg + self.rr[j]
        j = tf.to_float(tf.rint(tecg / self.h))

        k = i
        cond4 = lambda k0, i0, j0: k0 < j0 + tf.constant(1)
        body4 = lambda k0, i0, j0: self.ecg_calcs_while_body4(k0, i0, j0)
        tf.while_loop(cond4, body4, loop_vars=(k, i, j))
        i = j + 1

        return (i, j, Nrr, tecg)

    def ecg_calcs_while_body5(self, i, Nt, xt, yt, zt, x, timev):
        xt = xt[i].assign(x[1])
        yt = yt[i].assign(x[2])
        zt = zt[i].assign(x[3])
        self.RK4(x, self.sys_state_space_dims, timev, self.h, x)
        timev = timev + self.h
        return (i + 1, Nt, xt, yt, zt, x, timev)

    def ecg_calcs_while_body6(self, i, j, Nt, xts, yts, zts, xt, yt, zt, q):
        j += 1
        xts = xts[j].assign(xt[i])
        yts = yts[j].assign(yt[i])
        zts = zts[j].assign(zt[i])
        i += q
        return (i, j, Nt, xts, yts, zts, xt, yt, zt, q)

    def ecg_calcs_while_body7(self, i, Nts, zts, zmin, zmax):
        zmin = tf.cond(tf.less(zts[i], zmin), lambda: zts[i], lambda: zmin)
        zmax = tf.cond(tf.greater(zts[i], zmax), lambda: zts[i], lambda: zmax)
        return (i + 1, Nts, zts, zmin, zmax)

    def ecg_calcs_while_body8(self, i, Nts, zts, zmin, zrange):
        zts = zts[i].assign((zts[i] - zmin) * tf.constant(1.6) / zrange - tf.constant(0.4))
        return (i + 1, Nts, zts, zmin, zrange)

    def ecg_calcs_while_body9(self, i, Nts, zts):
        zts = zts[i].assign(zts[i] + self.ecg_params.getANoise() * (tf.constant(2.0) * self.ran1() - tf.constant(1.0)))
        return (i + 1, Nts, zts)

    def ecg_calcs_while_body10(self, i, Nts, tstep, zts, ipeak):
        self.ecg_result_time[i - 1] = (i - 1) * tstep
        self.ecg_result_voltage[i - 1] = zts[i]
        self.ecg_result_peaks[i - 1] = tf.to_int32(ipeak[i])
        return (i + 1, Nts, tstep, zts, ipeak)

    def run_ecg_calcs(self):
        """
        Run part of program.
        :return:
        """
        with tf.variable_scope("run_ecg_calcs"):
            RetValue = tf.constant(True, dtype=bool)


            # Perform some checks on input values:

            q = tf.to_int32(tf.rint(self.ecg_params.getSf() / self.ecg_params.getSfEcg()))
            qd = tf.to_float(self.ecg_params.getSf()) / tf.to_float(self.ecg_params.getSfEcg())

            # Convert angles from degrees to radians and copy a vector to ai:
            i1 = tf.constant(1)
            cond1 = lambda i0: i0 < tf.constant(6)
            body1 = lambda i0: self.ecg_calcs_while_body1(i0)
            tf.while_loop(cond1, body1, loop_vars=[i1])


            # Adjust extrema parameters for mean heart rate:
            hrfact = tf.sqrt(self.ecg_params.getHrMean() / tf.constant(60.0))
            hrfact2 = tf.sqrt(hrfact)

            i2 = tf.constant(1)
            cond2 = lambda i0, hrfact0: i0 < tf.constant(6)
            body2 = lambda i0, hrfact0: self.ecg_calcs_while_body2(i0,hrfact0)
            tf.while_loop(cond2, body2, loop_vars=(i2, hrfact))

            # Create a modified copy of theta_i
            edited_theta_i_1 = self.theta_i[1] * hrfact2
            edited_theta_i_2 = self.theta_i[2] * hrfact
            edited_theta_i_3 = self.theta_i[3] * tf.constant(1.0)
            edited_theta_i_4 = self.theta_i[4] * hrfact
            edited_theta_i_5 = self.theta_i[5] * tf.constant(1.0)
            self.theta_i = tf.stack([tf.constant(0.0), edited_theta_i_1, edited_theta_i_2, edited_theta_i_3, edited_theta_i_4, edited_theta_i_5])

            x = tf.stack([tf.constant(0.0), self.x_initial_value, self.y_initial_vale, self.z_initial_value])
            # Initialise seed:
            self.rseed = tf.constant(-1) * self.ecg_params.getSeed()

            # Calculate time scales:
            self.h = tf.constant(1.0) / tf.to_float(self.ecg_params.getSf())
            tstep = tf.constant(1.0) / tf.to_float(self.ecg_params.getSfEcg())

            # Calculate length of RR time series:
            rrmean = (tf.constant(60.0) / self.ecg_params.getHrMean())
            Nrr = tf.to_int32(tf.pow(tf.constant(2.0), tf.ceil(tf.log(self.ecg_params.getN() * rrmean * self.ecg_params.getSf()) /
                                                tf.log(tf.constant(2.0)))))

            '''
            print("Using " + str(Nrr) + " = 2^ " + str(int(math.log(1.0 * Nrr) / math.log(2.0))) +
                  " samples for calculating RR intervals")
            '''
            # Create rrprocess with required spectrum:
            # self.rr = tf.get_variable('rr', shape=[None]) # [0 for e in range(Nrr + 1)]  # new double[Nrr + 1]
            self.rr = []
            self.rrprocess(self.rr, self.ecg_params.getFLo(), self.ecg_params.getFHi(), self.ecg_params.getFLoStd(),
                      self.ecg_params.getFHiStd(), self.ecg_params.getLfHfRatio(), self.ecg_params.getHrMean(),
                      self.ecg_params.getHrStd(), self.ecg_params.getSf(), Nrr)

            # Create piecewise constant rr:
            self.rrpc = tf.get_variable('rrpc', shape=[None])  # [0 for e in range((2 * Nrr) + 1)] # new double[(2 * Nrr) + 1];
            tecg = tf.constant(0.0)
            i = tf.constant(1)
            j = tf.constant(1)
            cond3 = lambda i0, j0, Nrr0, tecg0 : i0 <= Nrr0
            body3 = lambda i0, j0, Nrr0, tecg0 : self.ecg_calcs_while_body3(i0, j0, Nrr0, tecg0)
            tf.while_loop(cond3, body3, loop_vars=(i, j, Nrr, tecg))

            Nt = j

            # Integrate dynamical system using fourth order Runge-Kutta:
            xt = tf.get_variable('xt', shape=[None])  # [0 for e in range(Nt + 1)] # new double[Nt + 1];
            yt = tf.get_variable('yt', shape=[None])  # [0 for e in range(Nt + 1)]  # new double[Nt + 1];
            zt = tf.get_variable('zt', shape=[None])  # [0 for e in range(Nt + 1)]  # new double[Nt + 1];
            timev = tf.constant(0.0)

            i2 = tf.constant(1)
            cond5 = lambda i0, Nt0, xt0, yt0, zt0, x0, timev0: i0 < Nt0 + tf.constant(1)
            body5 = lambda i0, Nt0, xt0, yt0, zt0, x0, timev0: self.ecg_calcs_while_body5(i0, Nt0, xt0, yt0, zt0, x0, timev0)
            tf.while_loop(cond5, body5, loop_vars=(i2, Nt, xt, yt, zt, x, timev))


            # Downsample to ECG sampling frequency:
            xts = tf.get_variable('xts', shape=[None])  # [0 for e in range(Nt + 1)]  # new double[Nt + 1];
            yts = tf.get_variable('ytt', shape=[None])  # [0 for e in range(Nt + 1)]  # new double[Nt + 1];
            zts = tf.get_variable('ztst', shape=[None])  # [0 for e in range(Nt + 1)]  # new double[Nt + 1];

            j = tf.constant(0)
            i = tf.constant(1)
            cond6 = lambda i0, j0, Nt0, xts0, yts0, zts0, xt0, yt0, zt0, q0: i0 <= Nt0
            body6 = lambda i0, j0, Nt0, xts0, yts0, zts0, xt0, yt0, zt0, q0: self.ecg_calcs_while_body6(i0, j0, Nt0, xts0, yts0, zts0, xt0, yt0, zt0, q0)
            tf.while_loop(cond6, body6, loop_vars=(i, j, Nt, xts, yts, zts, xt, yt, zt, q))

            Nts = j

            # Do peak detection using angle
            ipeak = tf.get_variable("ipeak", shape=[None])  # [0 for e in range(Nts + 1)]  # new double[Nts + 1];
            self.detect_peaks(ipeak, xts, yts, zts, Nts)

            # Scale signal to lie between -0.4 and 1.2 mV :
            zmin = zts[1]
            zmax = zts[1]

            i3 = tf.constant(2)
            cond7 = lambda i0, Nts0, zts0, zmin0, zmax0: i0 < Nts0 + tf.constant(1)
            body7 = lambda i0, Nts0, zts0, zmin0, zmax0: self.ecg_calcs_while_body7(i0, Nts0, zts0, zmin0, zmax0)
            tf.while_loop(cond7, body7, loop_vars=(i3, Nts, zts, zmin, zmax))

            zrange = zmax - zmin

            i4 = tf.constant(1)
            cond8 = lambda i0, Nts0, zts0, zmin0, zrange0: i0 < Nts0 + tf.constant(1)
            body8 = lambda i0, Nts0, zts0, zmin0, zrange0: self.ecg_calcs_while_body8(i0, Nts0, zts0, zmin0, zrange0)
            tf.while_loop(cond8, body8, loop_vars=(i4, Nts, zts, zmin, zrange))


            # Include additive uniformly distributed measurement noise:
            i5 = tf.constant(1)
            cond9 = lambda i0, Nts0, zts0: i0 < Nts0 + tf.constant(1)
            body9 = lambda i0, Nts0, zts0: self.ecg_calcs_while_body9(i0, Nts0, zts0)
            tf.while_loop(cond9, body9, loop_vars=(i5, Nts, zts))

            # Insert into the ECG data table:
            print("Generating result matrix...")

            self.ecg_result_num_rows = Nts

            self.ecg_result_time = tf.get_variable("ecg_result_time", shape=[None])  # [0 for e in range(self.ecg_result_num_rows)]
            self.ecg_result_voltage = tf.get_variable("ecg_result_voltage", shape=[None])  # [0 for e in range(self.ecg_result_num_rows)]
            self.ecg_result_peaks = tf.get_variable("ecg_result_peaks", shape=[None])  # [0 for e in range(self.ecg_result_num_rows)]

            i6 = tf.constant(1)
            cond10 = lambda i0, Nts0, tstep0, zts0, ipeak0: i0 < Nts0 + tf.constant(1)
            body10 = lambda i0, Nts0, tstep0, zts0, ipeak0: self.ecg_calcs_while_body10(i0, Nts0, tstep0, zts0, ipeak0)
            tf.while_loop(cond10, body10, loop_vars=(i6, Nts, tstep, zts, ipeak))

            print("Finished generating result matrix.")
            return (RetValue)